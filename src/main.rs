use std::{
    io::{stdout, Write},
    path::PathBuf,
    time::Instant,
};

use anyhow::bail;
use clap::{Parser, Subcommand};
use darkresource::Installation;

#[derive(Parser)]
#[command(about = "Interact with a Dark Mod or ID Tech 4 installation directory")]
struct Args {
    /// Operation to perform
    #[command(subcommand)]
    cmd: Command,

    /// Path to game installation. If not present, the current directory will be assumed.
    path: Option<String>,

    /// Do not fail if there are parsing errors, but parse whatever we can.
    #[arg(long, global = true)]
    ignore_errors: bool,
}

impl Args {
    // Get the installation directory
    fn get_path(&self) -> PathBuf {
        match &self.path {
            Some(s) => PathBuf::from(s),
            None => std::env::current_dir().expect("failed to get current directory"),
        }
    }
}

#[derive(Subcommand)]
enum Command {
    /// List available missions in the installation
    ListMissions,

    /// Get the contents of a specific file
    GetContents { file: String },

    /// List files matching a glob
    ListFiles {
        /// Glob to match, relative to base dir (e.g. "materials/*.mtr")
        glob: String,
    },

    /// List all defined materials
    ListMaterials,

    /// Get the preview image for a material.
    ///
    /// This will be the editor image if defined, otherwise the diffusemap.
    GetImagePreview { material: String },

    /// List available languages for string translation
    ListLanguages,

    /// Get the translation for a single string identifier
    GetString { identifier: String },
}

/// Main object for executing command-line requests
struct Executor {
    args: Args,
    installation: Installation,
}

impl Executor {
    // Construct and initialise
    fn new(args: Args, installation: Installation) -> Self {
        Self { args, installation }
    }

    // Dispatch command to command method
    fn dispatch(&self) -> anyhow::Result<()> {
        match &self.args.cmd {
            Command::ListMissions => self.list_missions(),
            Command::GetContents { file } => self.get_contents(file)?,
            Command::ListFiles { glob } => self.list_files(glob)?,
            Command::ListMaterials => self.list_materials(),
            Command::GetImagePreview { material } => self.get_image_preview(material)?,
            Command::ListLanguages => self.list_languages()?,
            Command::GetString { identifier } => self.get_string(identifier)?,
        }

        Ok(())
    }

    // "list-materials" command implementation
    fn list_materials(&self) {
        let time = Instant::now();
        let parsed = self.installation.get_materials();
        let duration = time.elapsed();
        if !parsed.errors.is_empty() && !self.args.ignore_errors {
            println!("Material parsing errors encounted:");
            for e in &parsed.errors {
                println!("{}", e);
            }
        } else {
            for (name, _) in &parsed.decls {
                println!("{}", name);
            }
        }
        println!(
            "{} parsed | {} errors | {:.2} msecs",
            parsed.decls.len(),
            parsed.errors.len(),
            duration.as_secs_f32() * 1000.0
        );
    }

    // "get-image-preview" command implementation
    fn get_image_preview(&self, material: &str) -> anyhow::Result<()> {
        let materials = self.installation.get_materials();
        if let Some(decl) = materials.decls.get(material) {
            if let Some(editor_image) = &decl.editor_image {
                println!("{}", editor_image);
            }
            Ok(())
        }
        else {
            bail!("No such material")
        }
    }

    // "list-missions" command implementation
    fn list_missions(&self) {
        println!("Installation: {}", &self.args.get_path().display());
        println!("Available missions:");
        for fm in self.installation.get_fms() {
            println!("  \"{}\" [{}]", fm.title, fm.path.display());
        }
    }

    // "list-files" command implementation
    fn list_files(&self, glob: &str) -> anyhow::Result<()> {
        let matching_files = self.installation.find_files(glob)?;
        for name in matching_files {
            println!("{}", name.display());
        }

        Ok(())
    }

    // "get-contents" command implementation
    fn get_contents(&self, file: &str) -> anyhow::Result<()> {
        let contents = self
            .installation
            .get_contents(file)
            .unwrap_or_else(|| panic!("file not found: {}", file));
        stdout().write_all(&contents)?;

        Ok(())
    }

    // "list-languages" command implementation
    fn list_languages(&self) -> anyhow::Result<()> {
        let langfiles = self.installation.find_files("strings/*.lang")?;
        for l in langfiles {
            // Extract the language name from the VFS path
            let filename = l
                .file_stem()
                .expect("missing filename")
                .to_str()
                .expect("failed to interpret filename");

            // Don't print the "all.lang" source file
            if filename != "all" {
                println!("{filename}");
            }
        }
        Ok(())
    }

    // "get-string" command implementation
    fn get_string(&self, identifier: &str) -> anyhow::Result<()> {
        println!("{}", self.installation.get_string(identifier)?);
        Ok(())
    }
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    // Canonicalise the path and create the Installation struct
    let abspath = std::fs::canonicalize(args.get_path())?;
    let ins = Installation::create_from_path(&abspath)?;

    // Execute the command
    let executor = Executor::new(args, ins);
    executor.dispatch()
}
