#![allow(dead_code)]
mod lang;
pub mod mtr;
mod parsecommon;

use std::{
    collections::{HashMap, HashSet},
    fmt::{self, Display},
    fs::File,
    io::{BufRead, BufReader, Read},
    path::{Path, PathBuf},
    sync::Mutex,
};

use anyhow::{anyhow, bail, Result};
use encoding::{all::ISO_8859_1, DecoderTrap, Encoding};
use glob::{glob, Pattern};
use lang::StringTable;
use mtr::ParsedMaterials;
use once_cell::sync::Lazy;
use parsecommon::ParseError;
use regex::Regex;
use zip::ZipArchive;

/// Top level representation of a game installation.
///
/// The installation contains a tree of resources, some or all of which may be contained within PK4
/// archives, and may contain individual mission directories with their own resource trees.
pub struct Installation {
    // Available FMs
    avail_fms: Vec<MissionInfo>,

    // Main resource dir
    base_resource_dir: ResourceDir,
}

impl Installation {
    /// Create new Installation from the given directory
    pub fn create_from_path(path: &Path) -> anyhow::Result<Installation> {
        Ok(Installation {
            avail_fms: find_missions_in_dir(path),
            base_resource_dir: ResourceDir::from_path(path)?,
        })
    }

    /// List available FMs in the installation
    pub fn get_fms(&self) -> &Vec<MissionInfo> {
        &self.avail_fms
    }

    /// Get contents for the specified file
    pub fn get_contents(&self, path: &str) -> Option<Vec<u8>> {
        self.base_resource_dir.get_file_contents(path)
    }

    /// Find all files matching a glob
    pub fn find_files(&self, search_pattern: &str) -> anyhow::Result<HashSet<PathBuf>> {
        self.base_resource_dir.find_files(search_pattern)
    }

    /// Return a list of all defined materials
    pub fn get_materials(&self) -> ParsedDecls<mtr::Material> {
        let matching_files = self.find_files("materials/*.mtr").unwrap();

        // Add materials parsed from each file in turn
        let mut result = ParsedDecls::new();
        for file in matching_files {
            // Get the file contents
            let contents = self.get_contents(file.to_str().unwrap()).unwrap();

            // Store whatever we were able to parse
            let filemats = ParsedMaterials::new_from_str(&String::from_utf8_lossy(&contents));
            for m in filemats.materials {
                result.decls.insert(m.name.clone(), m);
            }

            // If there was an error, record it
            if let Some(error) = filemats.last_error {
                result.errors.push(ErrorInfo { error, file });
            }
        }

        result
    }

    /// Return the translation for a given string identifier
    pub fn get_string(&self, identifier: &str) -> anyhow::Result<String> {
        // Load the language file
        let bytes = self.get_contents("strings/english.lang").ok_or_else(|| anyhow!("file not found"))?;
        let decoded = ISO_8859_1.decode(&bytes, DecoderTrap::Strict);
        match decoded {
            Ok(s) => {
                // Construct the StringTable and lookup the translation
                let table = StringTable::from_str(&s)?;
                Ok(table.get_string(identifier).map(String::from).unwrap_or_default())
            }
            Err(e) => Err(anyhow!("{}", e)),
        }
    }
}

/// Information about the nature and location of a parsing error.
#[derive(Debug, PartialEq, Eq)]
pub struct ErrorInfo {
    error: ParseError,
    file: PathBuf,
}

impl Display for ErrorInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.error {
            ParseError::NoMoreTokens => {
                write!(f, "Unexpected end of input [{}]", self.file.display())
            }
            ParseError::UnexpectedToken(tok, None) => {
                write!(f, "Unexpected token \"{}\" in file {}", tok, self.file.display())
            }
            ParseError::UnexpectedToken(tok, Some(range)) => {
                write!(
                    f,
                    "Unexpected token \"{}\" at character position {:?} in file {}",
                    tok,
                    range,
                    self.file.display()
                )
            }
        }
    }
}

/// Common interface for a decl of any type (material, sound shader etc)
pub trait Decl {
    /// Return the full name of this decl (including any virtual directories separated by
    /// forward slashes, if present)
    fn get_name(&self) -> &str;
}

/// Result of parsing decls of a particular type.
///
/// Contains a list of successfully parsed decls, along with information about the error which
/// terminated parsing, if any.
pub struct ParsedDecls<T: Decl> {
    /// List of successfully parsed decls.
    pub decls: HashMap<String, T>,

    /// Error(s) which terminated parsing, if any.
    pub errors: Vec<ErrorInfo>,
}

impl<T: Decl> ParsedDecls<T> {
    fn new() -> Self {
        ParsedDecls { decls: HashMap::new(), errors: Vec::new() }
    }
}

// A single PK4 archive
struct PK4 {
    // Path to the PK4 file on disk
    file_path: PathBuf,
    // The actual ZipArchive providing access to contents
    archive: ZipArchive<File>,
}

impl PK4 {
    // Return list of contained files
    fn file_names(&self) -> impl Iterator<Item = &str> {
        self.archive.file_names()
    }
}

type PK4Vec = Vec<Mutex<PK4>>;

fn find_pk4s_in_dir(dir: &Path) -> PK4Vec {
    // Use a glob to find *.pk4
    let mut pk4_pattern = PathBuf::from(dir);
    pk4_pattern.push("*.pk4");
    let pk4_files_iter =
        glob(pk4_pattern.to_str().expect("invalid pattern")).expect("invalid pattern");

    // Construct a PK4 object for each path we found
    let mut pk4_paths: PK4Vec = Vec::new();
    for path in pk4_files_iter.filter_map(Result::ok) {
        // Abort if we cannot open the PK4 file
        let file = File::open(&path)
            .unwrap_or_else(|_| panic!("failed to open PK4 file '{}'", path.display()));
        // Construct the PK4 data object
        let this_pk4 = PK4 {
            archive: ZipArchive::new(file)
                .unwrap_or_else(|_| panic!("failed to open PK4 archive '{}'", &path.display())),
            file_path: path,
        };
        pk4_paths.push(Mutex::new(this_pk4));
    }

    pk4_paths
}

// A tree of resource files, some of which may be contained within PK4 archives,
// unified into a single virtual file tree. A ResourceDir will exist for the
// base installation, with another separate ResourceDir containing the resource
// tree within a specific mission (if there is one).
struct ResourceDir {
    // Path to the resource directory itself. This must always be a real path —
    // the entire resource dir cannot be contained within a PK4, although files
    // in subdirectories may be.
    path: PathBuf,

    // PK4 Zip archives, stored in search order (i.e. the first archive to
    // contain a given file is the one whose file should be returned.)
    pk4_zips: PK4Vec,
}

impl ResourceDir {
    // Construct ResourceDir from the given path, scanning for any contained PK4
    // files.
    fn from_path(path: &Path) -> anyhow::Result<ResourceDir> {
        if !path.is_dir() {
            bail!("Unable to open directory: {}", path.display());
        } else {
            Ok(ResourceDir { path: path.to_owned(), pk4_zips: find_pk4s_in_dir(path) })
        }
    }

    // Search for a file in the PK4s, opening (and caching) each one in turn
    // until the file is found or all PK4s are examined.
    fn get_file_from_zips(&self, file_path: &str) -> Option<Vec<u8>> {
        for pk4 in &self.pk4_zips {
            if let Ok(mut file) = pk4.lock().unwrap().archive.by_name(file_path) {
                let mut outbuf = Vec::<u8>::new();
                file.read_to_end(&mut outbuf).unwrap_or_else(|_| {
                    panic!("failed to read compressed file contents: {}", file_path)
                });
                return Some(outbuf);
            }
        }

        // File not found
        None
    }

    // Return the contents of a given file within the resource dir, specified as
    // a relative path.
    fn get_file_contents(&self, file_path: &str) -> Option<Vec<u8>> {
        // First try to read from filesystem directly
        let full_path = self.path.join(file_path);
        if let Ok(contents) = std::fs::read(full_path) {
            Some(contents)
        } else {
            self.get_file_from_zips(file_path)
        }
    }

    // Return the names of all files matching a glob
    fn find_files(&self, search_glob: &str) -> anyhow::Result<HashSet<PathBuf>> {
        let mut result = HashSet::new();

        // First use glob to find matching files on the filesystem itself. The glob is assumed to
        // be relative (e.g. "materials/*.mtr"), so we need to prepend self.path for the search
        // operation, then make the results relative again for the returned data.
        let abs_glob = self.path.to_str().unwrap().to_owned() + "/" + search_glob;
        let glob_result = glob(&abs_glob)?;
        for path in glob_result.filter_map(Result::ok) {
            result.insert(path.strip_prefix(&self.path).unwrap().to_owned());
        }

        // Construct a Pattern from the glob string
        let pattern = Pattern::new(search_glob)?;

        // Examine each PK4 in search order
        for item in &self.pk4_zips {
            for file_name in item.lock().unwrap().file_names() {
                // Add a file name if it matches the pattern AND we haven't already seen it in a
                // higher-priority PK4
                if pattern.matches(file_name) {
                    let file_name_buf = PathBuf::from(file_name);
                    if !result.contains(&file_name_buf) {
                        result.insert(file_name_buf);
                    }
                }
            }
        }

        Ok(result)
    }
}

/// Metadata for an individual mission within the installation.
///
/// A mission is a separate resource tree contained within a named subdirectory
/// of the 'fms' directory in the main installation. Individual resources within
/// the mission tree may override and replace identically-named resources from
/// the main installation.
pub struct MissionInfo {
    /// User-visible title for the mission
    pub title: String,
    /// Installation path (absolute)
    pub path: PathBuf,
}

impl MissionInfo {
    // Attempt to construct a MissionInfo from the given directory path. Returns
    // None if the path does not contain a recognisable mission structure.
    fn from_path(path: &Path) -> Option<MissionInfo> {
        // Look for a darkmod.txt in the FM directory
        if let Ok(f) = File::open(path.join("darkmod.txt")) {
            // Read darkmod.txt line by line
            let reader = BufReader::new(f);
            for line_result in reader.lines() {
                let line = line_result.expect("failed to read line from darkmod.txt");

                // Look for known lines
                static NAME_RX: Lazy<Regex> =
                    Lazy::new(|| Regex::new(r"^\s*Title:\s*(\S.*\S)\s*$").unwrap());
                if let Some(cs) = NAME_RX.captures(&line) {
                    // Construct and return the MissionInfo object
                    return Some(MissionInfo {
                        title: String::from(cs.get(1).unwrap().as_str()),
                        path: PathBuf::from(path),
                    });
                }
            }
        }

        None
    }
}

fn find_missions_in_dir(dir: &Path) -> Vec<MissionInfo> {
    let mut fms: Vec<MissionInfo> = Vec::new();

    // Look for directories underneath the "fms" dir
    let mut fms_dir = PathBuf::from(dir);
    fms_dir.push("fms");
    if let Ok(entries) = std::fs::read_dir(&fms_dir) {
        for e in entries.filter_map(Result::ok) {
            if let Some(mission_info) = MissionInfo::from_path(&e.path()) {
                fms.push(mission_info);
            }
        }
    }

    fms
}

#[cfg(test)]
mod tests {
    use std::ffi::OsStr;

    use super::*;
    use more_asserts::assert_ge;
    use once_cell::sync::Lazy;
    use sha2::Digest;
    use sha2::Sha224;

    // Filesystem path to main testdata directory
    const MANIFEST_DIR: Lazy<PathBuf> = Lazy::new(|| PathBuf::from(env!("CARGO_MANIFEST_DIR")));
    const BASE_DIR: Lazy<PathBuf> = Lazy::new(|| MANIFEST_DIR.join("testdata/base"));

    // Actual ResourceDir for the testdata tree
    const RES_DIR: Lazy<ResourceDir> = Lazy::new(|| ResourceDir::from_path(&BASE_DIR).unwrap());

    #[test]
    fn create_installation_in_valid_dir() {
        let ins = Installation::create_from_path(&BASE_DIR);
        assert!(ins.is_ok());
    }

    #[test]
    fn create_installation_in_invalid_dir() {
        let fakedir = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("fakedir");

        // Attempting to create an Installation from a nonexistent location
        // should result in an error but not a panic.
        let ins = Installation::create_from_path(&fakedir);
        assert!(ins.is_err());
    }

    #[test]
    fn create_installation_with_no_fms() {
        let basedir = MANIFEST_DIR.join("testdata").join("base_no_fms");

        // Installation should be created successfully
        let ins = Installation::create_from_path(&basedir).expect("Installation not created");

        // No FMs should be present
        assert_eq!(ins.get_fms().len(), 0);
    }

    #[test]
    fn list_available_fms() {
        let ins = Installation::create_from_path(&BASE_DIR).unwrap();
        let fms = ins.get_fms();

        // Only fm directories with a darkmod.txt should appear in the result list
        assert_eq!(fms.len(), 2);
        assert!(fms.iter().any(|mi| mi.title == "Mission 2: Tears of St. Lucia"));
        assert!(fms.iter().any(|mi| mi.title == "testmission"));
        assert!(!fms.iter().any(|mi| mi.title == "not here"));
    }

    #[test]
    fn create_resource_dir() {
        assert_eq!(RES_DIR.path, BASE_DIR.to_owned());

        // ResourceDir should have found the PK4 files
        assert_eq!(RES_DIR.pk4_zips.len(), 2);
        assert_eq!(
            RES_DIR.pk4_zips[0].lock().unwrap().file_path.file_name(),
            Some(OsStr::new("cube_models.pk4"))
        );
        assert_eq!(
            RES_DIR.pk4_zips[1].lock().unwrap().file_path.file_name(),
            Some(OsStr::new("tdm_materials.pk4"))
        );
    }

    // Get SHA-224 hash of data as a string
    fn hash_string(contents: &Vec<u8>) -> String {
        format!("{:x}", Sha224::digest(contents))
    }

    #[test]
    fn get_contents_from_disk_file() {
        // Read the LWO file and check it has the correct size
        let resdir = ResourceDir::from_path(&BASE_DIR).unwrap();
        let contents = resdir.get_file_contents("models/cubes/Cube.lwo").unwrap();
        assert_eq!(contents.len(), 1656);

        // Check hash of file contents
        assert_eq!(
            hash_string(&contents),
            "4f033d66db2f5e97de02148d4b3f2eddf852fdafbe7d82f41637c4b2"
        );
    }

    #[test]
    fn get_contents_from_pk4_file() {
        // The ASE model is only in a PK4 archive
        let resdir = ResourceDir::from_path(&BASE_DIR).unwrap();
        let contents = resdir.get_file_contents("models/cubes/Cube.ase").unwrap();

        // Check size and contents
        assert_eq!(contents.len(), 8509);
        assert_eq!(
            hash_string(&contents),
            "2796cb5ecb56c42733ec4e9a878466124f499a7b1e4402ca42779936"
        );
    }

    #[test]
    fn find_files_matching_glob() {
        let resdir = ResourceDir::from_path(&BASE_DIR).unwrap();
        let matching_files = resdir.find_files("materials/*.mtr").unwrap();
        assert_eq!(matching_files.len(), 52);
        assert!(matching_files.contains(&PathBuf::from("materials/tdm_models_misc.mtr")));
        assert!(matching_files.contains(&PathBuf::from("materials/tdm_models_nautical.mtr")));
        assert!(matching_files.contains(&PathBuf::from("materials/tdm_nature_bones.mtr")));
        assert!(matching_files.contains(&PathBuf::from("materials/orbweaver.mtr")));
    }

    #[test]
    fn parse_all_materials() {
        let ins = Installation::create_from_path(&BASE_DIR).unwrap();
        let result = ins.get_materials();
        assert_ge!(result.decls.len(), 1485);
    }

    #[test]
    fn get_string() {
        let ins = Installation::create_from_path(&BASE_DIR).unwrap();
        let espanol = ins.get_string("#str_02462").expect("failed to get string");
        assert_eq!(espanol, "Español");
    }
}
