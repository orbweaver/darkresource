//! Code and data for parsing .lang (translation) files

use logos::Logos;
use std::collections::HashMap;

use crate::parsecommon::{ParseResult, GenericTokeniser};

mod parser {
    use super::*;

    const QUOTE: &str = "\"";

    // Remove quotes from a string and return the unquoted contents
    fn dequote(input: &str) -> String {
        assert!(input.len() >= 2);
        assert!(input.starts_with(QUOTE));
        assert!(input.ends_with(QUOTE));

        input[1..input.len() - 1].to_owned()
    }

    #[derive(Logos, Clone, PartialEq, Eq)]
    pub enum Token {
        // Punctuation
        #[token("{")]
        OpenBrace,
        #[token("}")]
        CloseBrace,

        // Translated string identifier
        #[regex(r##""#str_[^"]+""##, |lex| dequote(lex.slice()))]
        StringId(String),

        // An arbitrary string enclosed in double quotes
        #[regex(r#""[^"]*""#, |lex| dequote(lex.slice()))]
        QuotedString(String),

        // Whitespace, comments and errors
        #[error]
        #[regex(r"[ \t\n\r\f]+", logos::skip)]
        #[regex(r"//.*", logos::skip)]
        Error,
    }
}

/// Table of translated strings parsed from a .lang file
pub struct StringTable {
    // Map of string identifiers and their translations
    strings: HashMap<String, String>,
}

impl StringTable {
    /// Construct from a string
    pub fn from_str(s: &str) -> ParseResult<StringTable> {
        let mut tokeniser = GenericTokeniser::from_str(s);

        // First token must be '{'
        tokeniser.assert(parser::Token::OpenBrace)?;

        // Parse string translations in sequence
        let mut table = StringTable { strings: HashMap::new() };
        loop {
            match tokeniser.take()? {
                // Finish parsing on the final '}'
                parser::Token::CloseBrace => return Ok(table),

                // Parse string identifiers
                parser::Token::StringId(id) => {
                    if let parser::Token::QuotedString(s) = tokeniser.take()? {
                        table.strings.insert(id, s);
                    } else {
                        return Err(tokeniser.unexpected());
                    }
                }

                // Anything else is an error
                _ => return Err(tokeniser.unexpected()),
            }
        }
    }

    /// Get the translation for a specified string identifier
    pub fn get_string(&self, key: &str) -> Option<&str> {
        self.strings.get(key).map(|s| s.as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_LANG: &str = r##"
// String table - english (iso-8859-1)

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This file was generated automatically, DO NOT EDIT - all changes will !
// ! be lost when the file is regenerated. Edit strings/all.lang instead!  !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

{
	"#str_01019"	"''Metal is a symbol of man's dominion over the elements, and metal is heavily used in Builder clothing and architecture.''"
	"#str_01050"	"Tip: Hit the Use key after frobbing a body to shoulder it."
	"#str_01051"	"Tip: Tap the Use key quickly to drop a flashbomb at your feet where it won't blind you."
	"#str_02000"	"Error"
	"#str_02001"	"Warning"
	"#str_02002"	"Connection Error."
	"#str_02003"	"Code Logic Error"
	"#str_02004"	"No mission details loaded."
	"#str_02005"	"Mission Installation Failed"
	"#str_weapon_unarmed"	"Unarmed"
}
"##;

    #[test]
    fn construct_string_table() {
        let table = StringTable::from_str(TEST_LANG).unwrap();

        // Check table is populated
        assert_eq!(table.strings.len(), 10);

        // Check some string values
        assert_eq!(table.get_string("#str_02000"), Some("Error"));
        assert_eq!(table.get_string("#str_weapon_unarmed"), Some("Unarmed"));

        // Reject an invalid string
        assert!(StringTable::from_str("something else").is_err());
    }
}
