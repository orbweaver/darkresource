//! Data structures for representing material information
use std::collections::HashMap;

use self::expr::{MathExpr, Vector2};
use crate::{parsecommon::{self, ParseError}, Decl};
use enumset::{EnumSet, EnumSetType};

mod expr;
mod parser;

/// Available surface types for a material.
///
/// The surface type defines the footstep and projectile impact sounds for the material, and may
/// have other in-game effects (e.g. rope arrows only sticking into wood).
#[derive(Debug, PartialEq, Eq)]
pub enum SurfaceType {
    Flesh,
    Metal,
    Wood,
    Stone,
    Glass,
    Gravel,
    Water,
    Blood,
    Liquid,
    Plastic,
    Ricochet, // legacy D3
    SurfType15,
}

/// Light type of a material.
///
/// Most materials are designed for solid surfaces and will not have any light properties,
/// but certain materials are intended for ambient, fog or blend lights and will have the
/// respective keywords.
#[derive(PartialEq, Eq, Debug)]
pub enum LightType {
    Blend,
    Ambient,
    Fog,
}

/// Available material sort positions
#[derive(PartialEq, Eq, Debug, PartialOrd, Ord)]
pub enum SortPosition {
    SubView,
    Opaque,
    Decal,
    Far,
    Medium,
    Close,
    AlmostNearest,
    Nearest,
    PostProcess,
    Last,
    Custom(i32),
}

/// Types of surface deformation
#[derive(Debug, PartialEq)]
pub enum Deform {
    /// Rotate surface to face viewer (billboarding)
    Sprite,
    /// Rotate rectangle along its long axis
    Tube,
    /// Build a translucent border facing the viewer
    Flare(f32),
    /// Expand surface along its normals
    Expand(f32),
    /// Move surface along X axis
    Move(f32),
    /// Turbulently deform S and T values
    Turbulent(String, f64, f64, f64),
    /// Eyeball
    Eyeball,
    /// Emit particles from surface
    Particle(String),
    /// Emit particles from surface ignoring area
    Particle2(String),
}

/// Lookup table of arbitrary values for 1D animation
#[derive(Debug, PartialEq)]
pub struct Table {
    /// Name of the table
    name: String,
    /// Data values
    values: Vec<f64>,
    /// No interpolation between values
    snap: bool,
    /// Don't wrap around if index is out of range
    clamp: bool,
}

/// Possible frob stages
#[derive(PartialEq, Debug)]
pub enum FrobStage {
    /// No frob highlight
    None,
    /// Frob highlight uses diffuse stage with scale factor and white offset
    Diffuse(f64, f64),
    /// Frob highlight uses an explicit image map with scale factor and white offset
    ImageMap(String, f64, f64),
}

impl Default for FrobStage {
    fn default() -> Self {
        // Spec: https://forums.thedarkmod.com/index.php?/topic/21629-211-frobstages-automatic-generation-and-customization
        Self::Diffuse(0.15, 0.4)
    }
}

/// Static RGBA colour (in contrast to expr::RGBA which is four per-frame expressions)
#[derive(PartialEq, Debug)]
pub struct RGBA {
    r: f32,
    g: f32,
    b: f32,
    a: f32,
}

impl RGBA {
    fn white() -> Self {
        Self { r: 1.0, g: 1.0, b: 1.0, a: 1.0 }
    }

    fn black() -> Self {
        Self { r: 0.0, g: 0.0, b: 0.0, a: 1.0 }
    }
}

/// Decal info expression
#[derive(PartialEq, Debug)]
pub struct DecalInfo {
    /// How long the decal remains in place
    stay_secs: f32,
    /// How quickly the decal fades
    fade_secs: f32,
    /// Initial RGBA multiplier
    start_rgba: RGBA,
    /// Final RGBA multiplier
    end_rgba: RGBA,
}

/// Representation of a single material.
///
/// Each material has a number of global properties (such as surface type or noshadows), zero or
/// more render stages, and often a separate editor image for preview display.
#[derive(PartialEq, Debug)]
pub struct Material {
    /// Name of the material, e.g. `textures/stone/grey_stone1`.
    ///
    /// This is an arbitrary name which need not correspond to the paths of any image files. The
    /// only requirement is that it is unique for each material. Leading "directories" in the name
    /// have no significance except to suggest an organisation which front-end tools may choose to
    /// present to the user.
    pub name: String,

    /// User-visible description of the material.
    ///
    /// This has no in-game effect but may appear in editor GUIs.
    pub description: String,

    /// Light type, if present.
    ///
    /// Note that there is no keyword which can reliably distinguish a regular light (one
    /// which is not a blend, fog or ambient light) from a normal surface material. Both
    /// such materials will have a light_type value of None.
    pub light_type: Option<LightType>,

    /// Light falloff image, if specified
    pub light_falloff: Option<String>,

    /// List of stages in order of definition.
    ///
    /// Not every stage requires a separate render pass — diffuse and bump maps are declared as
    /// separate stages even though most render code will combine them in a single pass.
    pub stages: Vec<Stage>,

    /// Editor image, if any.
    pub editor_image: Option<String>,

    /// Frob highlight texture, if required
    pub frob_stage: FrobStage,

    /// Gui surface, if present
    pub gui_surf: Option<String>,

    /// Material surface type.
    pub surface_type: Option<SurfaceType>,

    /// Material sort position, if specified
    pub sort_position: Option<SortPosition>,

    /// Possible deformation of surface with this material
    pub deform: Option<Deform>,

    /// Material should not cast shadows
    pub no_shadows: bool,

    /// Material should not case shadows on itself (or other no_self_shadow materials)
    pub no_self_shadow: bool,

    /// Force casting of shadows
    pub force_shadows: bool,

    /// Material should render from both sides.
    pub two_sided: bool,

    /// Material should be forced opaque
    pub force_opaque: bool,

    /// Force overlays to be drawn over this material, even if translucent or alpha tested
    pub force_overlays: bool,

    /// Disable overlays over this material
    pub no_overlays: bool,

    /// Material is a collision surface
    pub is_collision: bool,

    /// Material is solid to players
    pub player_clip: bool,

    /// Physics slick flag
    pub slick: bool,

    /// Material is climbable
    pub is_climbable: bool,

    /// Global clamp mode (if not set on individual stages)
    pub clamp_mode: ClampMode,

    /// Material is non solid
    pub non_solid: bool,

    /// Material is translucent
    pub translucent: bool,

    /// Material does not generate impact sounds
    pub no_impact: bool,

    /// Use largest triangle for vertex normals instead of smoothing all
    pub unsmoothed_tangents: bool,

    /// Adjacent polygons with this material should not be merged
    pub discrete: bool,

    /// DMAP does not cut surface at BSP boundary
    pub no_fragment: bool,

    /// Do not fog this surface
    pub no_fog: bool,

    /// This fog volume will not fog out portals
    pub no_portal_fog: bool,

    /// Fog intensity for translucent surfaces
    pub fog_alpha: f32,

    /// Possible decal information
    pub decal_info: Option<DecalInfo>,

    /// Possible spectrum for this material (only shows under a light of same spectrum)
    pub spectrum: Option<i32>,

    /// Z offset for polygons with this material
    pub polygon_offset: f64,

    /// renderBump parameters for this material, if any
    pub render_bump: Option<RenderBump>,

    /// This material is a portal separator
    pub is_area_portal: bool,

    /// Disable CSG carving
    pub no_carve: bool,

    /// Solid to the AAS system
    pub aas_solid: bool,

    /// Obstacle to the AAS system
    pub aas_obstacle: bool,
}

impl Material {
    /// Construct with a given name and default values
    fn new(name: impl Into<String>) -> Material {
        Material {
            name: name.into(),
            description: String::new(),
            light_type: None,
            light_falloff: None,
            stages: vec![],
            editor_image: None,
            frob_stage: FrobStage::default(),
            gui_surf: None,
            surface_type: None,
            sort_position: None,
            deform: None,
            no_shadows: false,
            no_self_shadow: false,
            force_shadows: false,
            two_sided: false,
            force_opaque: false,
            force_overlays: false,
            no_overlays: false,
            is_collision: false,
            player_clip: false,
            slick: false,
            is_climbable: false,
            clamp_mode: ClampMode::None,
            non_solid: false,
            translucent: false,
            no_impact: false,
            unsmoothed_tangents: false,
            discrete: false,
            no_fragment: false,
            no_fog: false,
            no_portal_fog: false,
            fog_alpha: 0.0,
            decal_info: None,
            spectrum: None,
            polygon_offset: 0.0,
            render_bump: None,
            is_area_portal: false,
            no_carve: false,
            aas_solid: false,
            aas_obstacle: false,
        }
    }
}

impl Decl for Material {
    fn get_name(&self) -> &str {
        &self.name
    }
}

/// Possible ways for a texture to repeat or clamp outside of [0..1] texcoords
#[derive(Debug, Default, PartialEq, Eq)]
pub enum ClampMode {
    /// No clamping, repeat the texture infinitely
    #[default]
    None,
    /// Do not repeat the texture
    Clamp,
    /// Guarantee (0, 0, 0, 255) outside the texture bounds
    ZeroClamp,
}

/// Possible filtering for an image texture
#[derive(Debug, Default, PartialEq, Eq)]
pub enum ImageFilter {
    /// No filtering
    #[default]
    Nearest,
    /// Linear filtering
    Linear,
}

/// Set of channels which a Stage may write to
#[derive(EnumSetType, Debug)]
enum Channel {
    R,
    G,
    B,
    A,
}

/// Layout of image files for a particular image map
#[derive(Debug, PartialEq, Eq)]
enum ImageLayout {
    /// Standard 2D image
    Flat,
    /// Cube map (six images)
    Cube,
    /// Cube map in camera space
    CameraCube,
}

/// Image compression mode
#[derive(Debug, Default, PartialEq, Eq)]
enum ImageCompress {
    /// Normal (compressed in medium or low quality mode)
    #[default]
    Normal,
    /// High (compressed only in low quality mode)
    High,
    /// Forced high (never compressed)
    ForceHigh,
}

/// Texgen mode
#[derive(Debug, PartialEq)]
enum TexGen {
    Normal,
    Reflect,
    Skybox,
    WobbleSky(MathExpr, MathExpr, MathExpr),
}

/// Target size for stages which render into a buffer
#[derive(Debug, PartialEq, Eq)]
struct BufferSize {
    width: i32,
    height: i32,
}

/// renderBump parameters
#[derive(Debug, PartialEq)]
pub struct RenderBump {
    /// Width of the target texture
    width: i32,
    /// Height of the target texture
    height: i32,
    /// Antialiasing level (0 = 1x, 1 = 4x, 2 = 16x)
    aa_level: i32,
    /// Trace distance
    trace: f64,
    /// Source model
    model: String,
    /// Normal map output image
    image: String,
}

impl RenderBump {
    /// Construct with default values (from the idDevNet documentation)
    fn new() -> Self {
        Self {
            width: 256,
            height: 256,
            aa_level: 1,
            trace: 0.05,
            model: String::new(),
            image: String::new(),
        }
    }
}

/// Representation of a single material stage
#[derive(Debug)]
pub struct Stage {
    /// How this stage is composited with other stages
    blend_mode: BlendMode,

    /// Path to the texture image
    image_path: String,

    /// Image layout
    image_layout: ImageLayout,

    /// Image filtering
    image_filter: ImageFilter,

    /// Image compression mode
    image_compression: ImageCompress,

    /// Stage minimum value (e.g. for displacement or parallax map)
    min_value: f32,

    /// Stage maximum value (e.g. for displacement or parallax map)
    max_value: f32,

    /// Linear steps for parallax map
    linear_steps: i32,

    /// Ignore the image_downSize cvar
    no_pic_mip: bool,

    /// Whether and how this stage should be combined with vertex colors (if present)
    vertex_color_mode: VertexColorMode,

    /// 2D scale factor
    scale: Vector2,

    /// 2D scale factor around center position (0.5)
    center_scale: Vector2,

    /// 2D translation
    translate: Vector2,

    /// Rotation
    rotate: MathExpr,

    /// 2D shear
    shear: Vector2,

    /// Channels to write to
    channels: EnumSet<Channel>,

    /// Whether to write to the depth buffer
    depth_write: bool,

    /// Behaviour outside [0..1] texture coordinates
    clamp_mode: ClampMode,

    /// Clamp alpha to zero outside [0..1] coordinates (separate from RGB clamp_mode)
    alpha_zero_clamp: bool,

    /// Set the vertex color via an RGBA expression
    rgba: expr::RGBA,

    /// Texgen mode, if texture coordinates should be generated for this stage
    texgen: Option<TexGen>,

    /// Render this stage into a mirror map
    mirror_render_map: Option<BufferSize>,

    /// Render this stage as a camera (remote render map)
    remote_render_map: Option<BufferSize>,

    /// Render a video on this stage
    video_map: Option<VideoMap>,

    /// Possible custom vertex shader for this stage
    vertex_program: Option<String>,

    /// Possible custom fragment shader for this stage
    fragment_program: Option<String>,

    /// Map of vertex parms by index (possibly empty)
    vertex_parms: HashMap<i32, expr::Vector4>,

    /// Map of FragmentMap structures by index (possibly empty)
    fragment_maps: HashMap<i32, FragmentMap>,
}

impl PartialEq for Stage {
    /// Stage identity is determined by blend mode and image path. All other data is ancillary.
    fn eq(&self, other: &Self) -> bool {
        self.blend_mode == other.blend_mode && self.image_path == other.image_path
    }
}

impl Eq for Stage {}

impl Stage {
    /// Construct a default, empty stage
    pub fn new() -> Self {
        Self::with_blend_mode(BlendMode::Diffuse)
    }

    /// Create a stage with the given blend mode
    pub fn with_blend_mode(mode: BlendMode) -> Self {
        Stage {
            blend_mode: mode,
            image_path: String::default(),
            image_layout: ImageLayout::Flat,
            image_filter: ImageFilter::Linear,
            image_compression: ImageCompress::Normal,
            min_value: 0.0,
            max_value: 0.0,
            linear_steps: 0,
            no_pic_mip: false,
            vertex_color_mode: VertexColorMode::None,
            scale: Vector2::constant(1.0, 1.0),
            center_scale: Vector2::constant(1.0, 1.0),
            translate: Vector2::constant(0.0, 0.0),
            rotate: MathExpr::constant(0.0),
            shear: Vector2::constant(0.0, 0.0),
            channels: Channel::R | Channel::G | Channel::B | Channel::A,
            depth_write: true,
            clamp_mode: ClampMode::None,
            alpha_zero_clamp: false,
            rgba: expr::RGBA::white(),
            vertex_program: None,
            fragment_program: None,
            vertex_parms: HashMap::new(),
            fragment_maps: HashMap::new(),
            texgen: None,
            mirror_render_map: None,
            remote_render_map: None,
            video_map: None,
        }
    }
}

impl Default for Stage {
    fn default() -> Self {
        Self::new()
    }
}

/// Available blend modes
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BlendMode {
    Diffuse,
    Bump,
    Parallax,
    Specular,
    Add,
    Multiply,
    Blend,
    Custom(String, String),
}

/// Specifies how to combine vertex colour information with the material textures.
///
/// Models exported from 3D applications can have per-vertex colours embedded in the model file.
/// This value specifies if and how the vertex colour information should affect the final result.
#[derive(PartialEq, Eq, Debug)]
enum VertexColorMode {
    /// Ignore vertex colours.
    None,
    /// Multiply the vertex colour with the texture colour.
    Multiply,
    /// Subtract the vertex colour from RGB(1.0, 1.0, 1.0) and multiply the result with the texture
    /// colour.
    InverseMultiply,
}

/// Information about a single input map provided to a fragment program
#[derive(Debug, Eq, PartialEq)]
struct FragmentMap {
    /// Image identifier (input file or a special map like _currentRender)
    image_path: String,
    /// Filtering
    image_filter: ImageFilter,
    /// Compression mode
    image_compression: ImageCompress,
    /// Clamp mode
    clamp_mode: ClampMode,
}

impl FragmentMap {
    /// Construct with a name
    fn new_with_name(name: impl Into<String>) -> Self {
        Self {
            image_path: name.into(),
            image_filter: ImageFilter::Linear,
            image_compression: ImageCompress::Normal,
            clamp_mode: ClampMode::None,
        }
    }
}

/// Information about a video map
#[derive(Debug, Eq, PartialEq)]
struct VideoMap {
    /// Path to the video file
    video_path: String,
    /// Looping flag
    is_looping: bool,
}

impl VideoMap {
    /// Construct
    fn new(path: impl Into<String>, is_looping: bool) -> Self {
        Self { video_path: path.into(), is_looping }
    }
}

/// Result of parsing materials.
///
/// Contains a vector of all valid materials, along with an optional last error indicating why
/// parsing failed unexpectedly.
pub struct ParsedMaterials {
    pub materials: Vec<Material>,
    pub tables: Vec<Table>,
    pub last_error: Option<ParseError>,
}

impl ParsedMaterials {
    /// Construct an empty ParsedMaterials
    fn new() -> ParsedMaterials {
        ParsedMaterials { materials: Vec::new(), tables: Vec::new(), last_error: None }
    }

    /// Construct a ParsedMaterials by parsing all materials in the given string
    pub fn new_from_str(s: &str) -> ParsedMaterials {
        let mut lex = parsecommon::GenericTokeniser::from_str(s);
        ParsedMaterials::from_tokens(&mut lex)
    }
}

impl Default for ParsedMaterials {
    fn default() -> Self {
        Self::new()
    }
}
