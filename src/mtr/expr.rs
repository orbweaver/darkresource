use std::cell::Cell;

type MathExprRef = Box<MathExpr>;

/// Representation of a parsed math expression, e.g. "parm11 * 0.5"
#[derive(Clone, Debug)]
pub enum MathExpr {
    /// Store a constant float value
    Constant(f64),
    /// Retrieve named input
    Input(String),
    /// Add two subexpressions
    Add(MathExprRef, MathExprRef),
    /// Subtract two subexpressions
    Subtract(MathExprRef, MathExprRef),
    /// Multiply two subexpressions
    Multiply(MathExprRef, MathExprRef),
    /// Divide two subexpressions
    Divide(MathExprRef, MathExprRef),
    /// Return the numeric maximum of two subexpressions
    Max(MathExprRef, MathExprRef),
    /// Boolean OR
    Or(MathExprRef, MathExprRef),
    /// Look up a value in a table
    Table { name: String, time_pos: MathExprRef },
}

/// Provider of input values (e.g. "time" or "parm0") when evaluating a MathExpr
pub trait ValueProvider {
    /// Get the value of the named parameter
    fn get_value(&self, name: &str) -> f64;

    /// Get the value of the given table at the given time position
    fn get_table_value(&self, table_name: &str, time_pos: f64) -> f64;
}

impl MathExpr {
    /// Construct a MathExpr containing a constant value
    pub fn constant(value: f64) -> MathExpr {
        MathExpr::Constant(value)
    }

    /// Construct a MathExpr which adds two sub-expressions
    pub fn add(lhs: MathExpr, rhs: MathExpr) -> MathExpr {
        MathExpr::Add(MathExprRef::new(lhs), MathExprRef::new(rhs))
    }

    /// Construct a MatchExpr which subtracts two sub-expressions
    pub fn subtract(left: MathExpr, right: MathExpr) -> MathExpr {
        MathExpr::Subtract(MathExprRef::new(left), MathExprRef::new(right))
    }

    /// Construct a MathExpr which multiplies two sub-expressions
    pub fn multiply(left: MathExpr, right: MathExpr) -> MathExpr {
        MathExpr::Multiply(MathExprRef::new(left), MathExprRef::new(right))
    }

    /// Construct a MathExpr which divides two sub-expressions
    pub fn divide(left: MathExpr, right: MathExpr) -> MathExpr {
        MathExpr::Divide(MathExprRef::new(left), MathExprRef::new(right))
    }

    /// Construct a MathExpr which returns the maximum of two sub-expressions
    pub fn max(left: MathExpr, right: MathExpr) -> MathExpr {
        MathExpr::Max(MathExprRef::new(left), MathExprRef::new(right))
    }

    /// Construct a MathExpr which evaluates the OR function
    pub fn or(left: MathExpr, right: MathExpr) -> MathExpr {
        MathExpr::Or(MathExprRef::new(left), MathExprRef::new(right))
    }

    /// Construct a MathExpr which requests an input parameter
    pub fn input(name: impl Into<String>) -> MathExpr {
        MathExpr::Input(name.into())
    }

    /// Construct a MathExpr which performs a table lookup
    pub fn table(name: impl Into<String>, time_pos: MathExpr) -> MathExpr {
        MathExpr::Table { name: name.into(), time_pos: MathExprRef::new(time_pos) }
    }

    // Recursively obtain the value of an op in the tree
    fn evaluate(op: &MathExpr, valprov: &impl ValueProvider) -> f64 {
        match op {
            MathExpr::Constant(value) => *value,
            MathExpr::Input(param) => valprov.get_value(param),
            MathExpr::Add(l, r) => {
                Self::evaluate(l, valprov) + Self::evaluate(r, valprov)
            }
            MathExpr::Subtract(l, r) => {
                Self::evaluate(l, valprov) - Self::evaluate(r, valprov)
            }
            MathExpr::Multiply(l, r) => {
                Self::evaluate(l, valprov) * Self::evaluate(r, valprov)
            }
            MathExpr::Divide(l, r) => {
                Self::evaluate(l, valprov) / Self::evaluate(r, valprov)
            }
            MathExpr::Max(l, r) => {
                Self::evaluate(l, valprov).max(Self::evaluate(r, valprov))
            }
            MathExpr::Or(l, r) => {
                let l = Self::evaluate(l, valprov);
                if l != 0.0 {
                    l
                } else {
                    Self::evaluate(r, valprov)
                }
            }
            MathExpr::Table { name, time_pos } => {
                valprov.get_table_value(&name, Self::evaluate(time_pos, valprov))
            }
        }
    }

    /// Get the final value of this expression
    pub fn value(&self, valprov: &impl ValueProvider) -> f64 {
        Self::evaluate(&self, valprov)
    }

    /// Get the final value of this expression, if (and only if) it can be produced without
    /// requesting any input parameters.
    ///
    /// This may be useful in tests or for client code which does not have a mechanism to
    /// supply input parameters but wants to evaluate expressions on a best-effort basis.
    pub fn try_value(&self) -> Option<f64> {
        // Detect any requests for input parameters
        struct ValReqDetector {
            value_requested: Cell<bool>,
        }
        impl ValueProvider for ValReqDetector {
            fn get_value(&self, _: &str) -> f64 {
                self.value_requested.replace(true);
                0.0
            }
            fn get_table_value(&self, _: &str, _: f64) -> f64 {
                self.get_value("")
            }
        }

        // If a parameter was requested return None, otherwise return the calculated value
        let detector = ValReqDetector { value_requested: Cell::new(false) };
        let maybe_val = self.value(&detector);
        if detector.value_requested.get() {
            None
        } else {
            Some(maybe_val)
        }
    }
}

impl PartialEq for MathExpr {
    /// Two math expressions are equal if the root op is the same, and both subtrees (if
    /// any) are recursively equal. Two constant or param lookup ops are equal if the
    /// constant value or parameter name is the same.
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (MathExpr::Constant(l), MathExpr::Constant(r)) => l == r,
            (MathExpr::Input(l), MathExpr::Input(r)) => l == r,
            (MathExpr::Add(l1, l2), MathExpr::Add(r1, r2)) => l1 == r1 && l2 == r2,
            (MathExpr::Subtract(l1, l2), MathExpr::Subtract(r1, r2)) => l1 == r1 && l2 == r2,
            (MathExpr::Multiply(l1, l2), MathExpr::Multiply(r1, r2)) => l1 == r1 && l2 == r2,
            (
                MathExpr::Table { name: lname, time_pos: ltime },
                MathExpr::Table { name: rname, time_pos: rtime },
            ) => lname == rname && ltime == rtime,
            _ => false,
        }
    }
}

impl Eq for MathExpr {}

/// Four math expressions representing an RGBA colour value
#[derive(Debug, Eq, PartialEq)]
pub struct RGBA {
    pub r: MathExpr,
    pub g: MathExpr,
    pub b: MathExpr,
    pub a: MathExpr,
}

impl RGBA {
    /// Return an RGBA expression representing constant white
    pub fn white() -> RGBA {
        Self {
            r: MathExpr::constant(1.0),
            g: MathExpr::constant(1.0),
            b: MathExpr::constant(1.0),
            a: MathExpr::constant(1.0),
        }
    }
}

/// Two math expressions representing a 2D vector
#[derive(Debug)]
pub struct Vector2 {
    pub x: MathExpr,
    pub y: MathExpr,
}

impl Vector2 {
    /// Construct with constant values
    pub fn constant(x: f64, y: f64) -> Self {
        Self { x: MathExpr::constant(x), y: MathExpr::constant(y) }
    }
}

/// Four math expressions representing a 3D homogeneous vector
#[derive(Debug)]
pub struct Vector4 {
    pub x: MathExpr,
    pub y: MathExpr,
    pub z: MathExpr,
    pub w: MathExpr,
}

impl Vector4 {
    /// Attempt to get the value of the whole vector
    pub fn try_value(&self) -> Option<[f64; 4]> {
        let x = self.x.try_value();
        let y = self.y.try_value();
        let z = self.z.try_value();
        let w = self.w.try_value();

        if let (Some(xv), Some(yv), Some(zv), Some(wv)) = (x, y, z, w) {
            Some([xv, yv, zv, wv])
        } else {
            None
        }
    }
}

pub type ImageExprRef = Box<ImageExpr>;

/// Representation of an image map expression, such as "addnormals(x, y)" or "heightmap(x)"
#[derive(Debug, Eq, PartialEq)]
pub enum ImageExpr {
    /// Basic image path (leaf of the expression)
    ImagePath(String),
    /// addnormals(image, image)
    AddNormals(ImageExprRef, ImageExprRef),
    /// heightmap(image, float)
    HeightMap(ImageExprRef, MathExpr),
    /// scale(image, r, g, b, a)
    Scale(ImageExprRef, RGBA),
    /// invertColor(image)
    InvertColor(ImageExprRef),
    /// invertAlpha(image)
    InvertAlpha(ImageExprRef),
    /// makeAlpha(image)
    MakeAlpha(ImageExprRef),
    /// makeIntensity(image)
    MakeIntensity(ImageExprRef),
    /// cameraLayout(image)
    CameraLayout(ImageExprRef),
}

impl ImageExpr {
    /// Return a list of image paths required by this expression
    pub fn get_images(&self) -> Vec<String> {
        let mut images = Vec::new();
        match self {
            ImageExpr::ImagePath(path) => images.push(path.clone()),
            ImageExpr::AddNormals(expr_l, expr_r) => {
                images.append(&mut expr_l.get_images());
                images.append(&mut expr_r.get_images());
            }
            ImageExpr::HeightMap(image_expr, _) => {
                images.append(&mut image_expr.get_images());
            }
            ImageExpr::Scale(expr, _) => {
                images.append(&mut expr.get_images());
            }
            ImageExpr::InvertColor(expr) => {
                images.append(&mut expr.get_images());
            }
            ImageExpr::InvertAlpha(expr) => {
                images.append(&mut expr.get_images());
            }
            ImageExpr::MakeAlpha(expr) => {
                images.append(&mut expr.get_images());
            }
            ImageExpr::MakeIntensity(expr) => {
                images.append(&mut expr.get_images());
            }
            ImageExpr::CameraLayout(expr) => {
                images.append(&mut expr.get_images());
            }
        }
        images
    }
}

#[cfg(test)]
mod tests {
    use std::{cell::RefCell, collections::HashMap};

    use super::*;

    /// Test struct which counts the number of times a ValueFn is called
    struct ValReqCounter {
        counts: RefCell<HashMap<String, i32>>,
    }

    impl ValReqCounter {
        fn new() -> ValReqCounter {
            ValReqCounter { counts: RefCell::new(HashMap::new()) }
        }

        fn is_empty(&self) -> bool {
            self.counts.borrow().is_empty()
        }

        fn get_count(&self, param: &str) -> i32 {
            *self.counts.borrow().get(param).unwrap_or(&0)
        }
    }

    impl ValueProvider for ValReqCounter {
        fn get_value(&self, name: &str) -> f64 {
            // Update the request count map
            let mut counts = self.counts.borrow_mut();
            if let Some(value) = counts.get_mut(name) {
                *value += 1;
            } else {
                counts.insert(name.to_owned(), 1);
            }

            // Dummy constant return value
            1.0
        }

        fn get_table_value(&self, _: &str, _: f64) -> f64 {
            3.0
        }
    }

    #[test]
    fn evaluate_constant() {
        let counter = ValReqCounter::new();
        let constant = MathExpr::constant(1.2388);
        assert_eq!(constant.value(&counter), 1.2388);
        assert_eq!(constant.try_value(), Some(1.2388));
        assert!(counter.is_empty());
    }

    #[test]
    fn evaluate_add() {
        let counter = ValReqCounter::new();

        // Add two constants
        {
            let c1 = MathExpr::constant(25.6);
            let c2 = MathExpr::constant(20.1);
            let sum = MathExpr::add(c1, c2);
            assert_eq!(sum.value(&counter), 45.7);
            assert_eq!(sum.try_value(), Some(45.7));
        }

        // Add three constants
        {
            let c1 = MathExpr::constant(5.0);
            let c2 = MathExpr::constant(11.0);
            let c3 = MathExpr::constant(15.5);

            let lhs = MathExpr::add(c1, c2);
            let sum = MathExpr::add(lhs, c3);
            assert_eq!(sum.value(&counter), 31.5);
            assert_eq!(sum.try_value(), Some(31.5));
        }
        assert!(counter.is_empty());
    }

    #[test]
    fn evaluate_subtract() {
        let c1 = MathExpr::constant(18.25);
        let c2 = MathExpr::constant(14.0);
        let diff = MathExpr::subtract(c1, c2);
        assert_eq!(diff.try_value(), Some(4.25));
    }

    #[test]
    fn evaluate_multiply() {
        let valueprov = ValReqCounter::new();

        // Multiply two constants
        {
            let c1 = MathExpr::constant(4.5);
            let c2 = MathExpr::constant(2.0);
            let prod = MathExpr::multiply(c1, c2);
            assert_eq!(prod.value(&valueprov), 9.0);
            assert_eq!(prod.try_value(), Some(9.0));
        }
    }

    #[test]
    fn evaluate_divide() {
        let c1 = MathExpr::constant(350.0);
        let c2 = MathExpr::constant(5.0);
        let diff = MathExpr::divide(c1, c2);
        assert_eq!(diff.try_value(), Some(70.0));
    }

    #[test]
    fn evaluate_max() {
        let c1= MathExpr::constant(57.5);
        let c2 = MathExpr::constant(129.6);
        let m = MathExpr::max(c1, c2);
        assert_eq!(m.try_value(), Some(129.6));
    }

    #[test]
    fn evalute_or() {
        let fifteen = MathExpr::constant(15.0);
        let zero = MathExpr::constant(0.0);
        assert_eq!(MathExpr::or(fifteen, zero).try_value(), Some(15.0));

        let minus_one = MathExpr::constant(-1.0);
        let eleven = MathExpr::constant(11.0);
        assert_eq!(MathExpr::or(minus_one, eleven).try_value(), Some(-1.0));

        let fifty = MathExpr::constant(50.0);
        assert_eq!(MathExpr::or(MathExpr::constant(0.0), fifty).try_value(), Some(50.0));
    }

    #[test]
    fn evaluate_input() {
        let counter = ValReqCounter::new();
        let input = MathExpr::input("parm11");
        assert_eq!(input.value(&counter), 1.0);
        assert_eq!(counter.get_count("parm11"), 1);
    }

    #[test]
    fn evaluate_add_input() {
        let input = MathExpr::input("parm1");
        let constant = MathExpr::constant(-128.5);

        // Add the input and the constant
        let values = ValReqCounter::new();
        let result = MathExpr::add(input, constant);
        assert_eq!(result.value(&values), -127.5);
        assert_eq!(values.get_count("parm1"), 1);
    }

    #[test]
    fn evaluate_table() {
        let table = MathExpr::table("sintable", MathExpr::constant(4.5));

        // Table lookup always requires a value provider
        assert_eq!(table.try_value(), None);

        // Lookup should call the get_table_value method on the provider
        struct Doubler {}
        impl ValueProvider for Doubler {
            fn get_value(&self, _: &str) -> f64 {
                panic!("get_value() called from table lookup")
            }
            fn get_table_value(&self, _: &str, time_pos: f64) -> f64 {
                time_pos * 2.0
            }
        }

        // Doubler should double the time_pos value, which is 4.5
        let prov = Doubler {};
        assert_eq!(table.value(&prov), 9.0);
    }

    #[test]
    fn mathexpr_equality() {
        // Constants
        let one = MathExpr::constant(1.0);
        let another_one = MathExpr::constant(1.0);
        let two = MathExpr::constant(2.0);
        assert!(one == one);
        assert!(one == another_one);
        assert!(one != two);

        // Variable input
        let parm0 = MathExpr::input("parm0");
        let parm1 = MathExpr::input("parm1");
        let another_parm1 = MathExpr::input("parm1");
        assert!(parm0 == parm0);
        assert!(parm0 != parm1);
        assert!(parm1 == another_parm1);
    }
}
