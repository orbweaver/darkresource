//! Parsing functions for material definitions
#![allow(dead_code)]

use crate::parsecommon::{GenericTokeniser, ParseResult};

use super::{
    expr::{ImageExpr, ImageExprRef, MathExpr, Vector2, Vector4},
    *,
};
use enumset::enum_set;
use logos::Logos;

/// Trait for an object which can be constructed by parsing tokens
trait Parseable {
    /// Parse from tokens
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Self> where Self: Sized;

    /// Convenience method to parse from a string by constructing a Tokeniser
    fn from_str(s: &str) -> ParseResult<Self> where Self: Sized {
        let mut tok = Tokeniser::from_str(s);
        Self::from_tokens(&mut tok)
    }
}

#[derive(Clone, Logos, Debug, PartialEq)]
pub enum Token {
    // Punctuation
    #[token("{")]
    OpenBrace,
    #[token("}")]
    CloseBrace,
    #[token("(")]
    OpenParen,
    #[token(")")]
    CloseParen,
    #[token("[")]
    OpenBracket,
    #[token("]")]
    CloseBracket,
    #[token(",")]
    Comma,

    // Math expressions
    #[regex("[0-9.]+", |num| num.slice().parse(), priority = 5)]
    Number(f64),
    #[regex("parm[0-9]+|global[0-7]|time|sound", |lex| lex.slice().to_owned(), ignore(case))]
    Parm(String),
    #[token("*")]
    Times,
    #[token("+")]
    Plus,
    #[token("-")]
    Minus,
    #[token("/")]
    Divide,
    #[token("||")]
    Or,

    // Possible name for a material. Because this is such a generic pattern, most unexpected
    // words will end up as this.
    #[regex("[A-Za-z0-9_/.\\\\]+", |lex| lex.slice().to_owned())]
    Name(String),

    #[token("description")]
    Description,

    // An arbitrary string enclosed in double quotes (no handling of escaping at the moment)
    #[regex(r#""[^"]*""#, |lex| lex.slice().trim_matches('"').to_owned())]
    QuotedString(String),

    #[token("if")]
    If,

    // Blend modes
    #[token("blend")]
    Blend,
    #[token("diffusemap", ignore(case))]
    DiffuseMap,
    #[token("bumpmap")]
    BumpMap,
    #[token("parallaxmap", ignore(case))]
    ParallaxMap,
    #[token("specularmap")]
    SpecularMap,
    #[token("add")]
    Add,
    #[token("filter")]
    Filter,
    #[token("modulate")]
    Modulate,

    // Image maps
    #[token("map")]
    Map,
    #[token("qer_editorimage", ignore(case))]
    EditorImage,
    #[token("addnormals")]
    AddNormals,
    #[token("heightmap")]
    HeightMap,
    #[token("makeAlpha", ignore(case))]
    MakeAlpha,
    #[token("makeIntensity", ignore(case))]
    MakeIntensity,
    #[token("invertColor", ignore(case))]
    InvertColor,
    #[token("invertAlpha", ignore(case))]
    InvertAlpha,
    #[token("cameraLayout", ignore(case))]
    CameraLayout,
    #[token("cubeMap", ignore(case))]
    CubeMap,
    #[token("cameraCubeMap", ignore(case))]
    CameraCubeMap,

    // Image filtering and compression
    #[token("linear")]
    Linear,
    #[token("nopicmip", ignore(case))]
    NoPicMip,
    #[token("uncompressed", ignore(case))]
    #[token("highQuality", ignore(case))]
    HighQuality,
    #[token("forceHighQuality", ignore(case))]
    ForceHiQuality,

    // Custom blend modes
    #[regex("[Gg][Ll]_[A-Za-z_]+", |lex| lex.slice().to_owned())]
    GlCustomBlend(String),

    // Surface types
    #[token("flesh", ignore(case))]
    Flesh,
    #[token("metal", ignore(case))]
    Metal,
    #[token("wood", ignore(case))]
    Wood,
    #[token("stone", ignore(case))]
    Stone,
    #[token("glass", ignore(case))]
    Glass,
    #[token("gravel", ignore(case))]
    Gravel,
    #[token("water", ignore(case))]
    Water,
    #[token("blood", ignore(case))]
    Blood,
    #[token("liquid", ignore(case))]
    Liquid,
    #[token("plastic", ignore(case))]
    Plastic,
    #[token("surftype15", ignore(case))]
    SurfType15,
    #[token("ladder", ignore(case))]
    Ladder,
    #[token("ricochet", ignore(case))]
    Ricochet,

    // Material properties
    #[token("noshadows", ignore(case))]
    NoShadows,
    #[token("noselfshadow", ignore(case))]
    NoSelfShadow,
    #[token("forceShadows", ignore(case))]
    ForceShadows,
    #[token("twosided", ignore(case))]
    TwoSided,
    #[token("forceopaque", ignore(case))]
    ForceOpaque,
    #[token("nonsolid", ignore(case))]
    NonSolid,
    #[token("solid", ignore(case))]
    Solid,
    #[token("translucent")]
    Translucent,
    #[token("noimpact")]
    NoImpact,
    #[token("noFog", ignore(case))]
    NoFog,
    #[token("noPortalFog", ignore(case))]
    NoPortalFog,
    #[token("fogAlpha", ignore(case))]
    FogAlpha,
    #[token("unsmoothedTangents", ignore(case))]
    UnsmoothedTangents,
    #[token("forceOverlays", ignore(case))]
    ForceOverlays,
    #[token("noOverlays", ignore(case))]
    NoOverlays,
    #[token("discrete")]
    Discrete,
    #[token("noFragment", ignore(case))]
    NoFragment,
    #[token("collision")]
    Collision,
    #[token("playerclip", ignore(case))]
    PlayerClip,
    #[token("slick")]
    Slick,
    #[token("polygonOffset", ignore(case))]
    PolygonOffset,
    #[token("DECAL_MACRO")]
    DecalMacro,
    #[token("TWOSIDED_DECAL_MACRO")]
    TwoSidedDecalMacro,
    #[token("decalInfo")]
    DecalInfo,
    #[token("PARTICLE_MACRO")]
    ParticleMacro,
    #[token("GLASS_MACRO")]
    GlassMacro,
    #[token("spectrum")]
    Spectrum,
    #[token("guiSurf", ignore(case))]
    GuiSurf,
    #[token("areaportal")]
    AreaPortal,
    #[token("qer_nocarve")]
    NoCarve,
    #[token("aassolid")]
    AasSolid,
    #[token("aasobstacle")]
    AasObstacle,

    // Frob stages
    #[token("frobstage_texture")]
    FrobStageTexture,
    #[token("frobstage_diffuse")]
    FrobStageDiffuse,
    #[token("frobstage_none")]
    FrobStageNone,

    // Light properties
    #[token("blendLight", ignore(case))]
    BlendLight,
    #[token("fogLight", ignore(case))]
    FogLight,
    #[token("ambientLight", ignore(case))]
    AmbientLight,
    #[token("lightFalloffImage", ignore(case))]
    LightFalloff,

    // Material sorting
    #[token("sort")]
    Sort,
    #[token("decal")]
    Decal,
    #[token("far")]
    Far,
    #[token("medium")]
    Medium,
    #[token("close")]
    Close,
    #[token("nearest")]
    Nearest,
    #[token("postProcess")]
    PostProcess,
    #[token("last")]
    Last,

    // Material deforms
    #[token("deform")]
    Deform,
    #[token("sprite")]
    Sprite,
    #[token("flare")]
    Flare,
    #[token("tube")]
    Tube,
    #[token("eyeball")]
    Eyeball,
    #[token("particle")]
    Particle,
    #[token("particle2")]
    Particle2,
    #[token("turbulent")]
    Turbulent,

    // Stage properties
    #[token("rgb")]
    Rgb,
    #[token("red")]
    Red,
    #[token("green")]
    Green,
    #[token("blue")]
    Blue,
    #[token("alpha")]
    Alpha,
    #[token("color")]
    Color,
    #[token("alphaTest", ignore(case))]
    AlphaTest,
    #[token("maskColor", ignore(case))]
    MaskColor,
    #[token("maskAlpha", ignore(case))]
    MaskAlpha,
    #[token("maskDepth", ignore(case))]
    MaskDepth,
    #[token("colored")]
    Colored,
    #[token("min")]
    Min,
    #[token("max")]
    Max,
    #[token("linearSteps", ignore(case))]
    LinearSteps,

    // Texgens
    #[token("texgen", ignore(case))]
    TexGen,
    #[token("normal", ignore(case))]
    Normal,
    #[token("reflect")]
    Reflect,
    #[token("skybox")]
    SkyBox,
    #[token("wobbleSky", ignore(case))]
    WobbleSky,

    // Stage transforms
    #[token("scale")]
    Scale, // also a possible map expression scale(<map>, ...)
    #[token("centerScale", ignore(case))]
    CenterScale,
    #[token("translate")]
    Translate,
    #[token("scroll")]
    Scroll,
    #[token("rotate")]
    Rotate,
    #[token("shear")]
    Shear,

    // Vertex color blending
    #[token("vertexColor", ignore(case))]
    VertexColor,
    #[token("inverseVertexColor", ignore(case))]
    InverseVertexColor,

    // Texture clamping
    #[token("clamp")]
    Clamp,
    #[token("zeroClamp", ignore(case))]
    ZeroClamp,
    #[token("alphaZeroClamp", ignore(case))]
    AlphaZeroClamp,

    // Stage shader programs
    #[token("vertexProgram", ignore(case))]
    VertexProgram,
    #[token("fragmentProgram", ignore(case))]
    FragmentProgram,
    #[token("program", ignore(case))]
    Program,
    #[token("vertexParm", ignore(case))]
    VertexParm,
    #[token("fragmentMap", ignore(case))]
    FragmentMap,

    // Render maps
    #[token("mirrorRenderMap", ignore(case))]
    MirrorRenderMap,
    #[token("remoteRenderMap", ignore(case))]
    RemoteRenderMap,
    #[token("videoMap", ignore(case))]
    VideoMap,
    #[token("loop")]
    Loop,

    // Lookup tables
    #[token("table")]
    Table,
    #[token("snap")]
    Snap,

    // renderBump expressions
    #[token("renderBump", ignore(case))]
    RenderBump,
    #[token("-size")]
    RenderBumpSize,
    #[token("-aa")]
    RenderBumpAA,
    #[token("-trace")]
    RenderBumpTrace,
    #[token("-colorMap")]
    RenderBumpColorMap,

    #[error]
    #[regex(r"[ \t\n\r\f]+", logos::skip)]
    #[regex(r"//.*", logos::skip)] // C++ comments
    #[regex(r"/\*[^*]*\*+(?:[^/*][^*]*\*+)*/", logos::skip)]
    // C comments (including multi-line)
    Error,
}

type Tokeniser<'s> = GenericTokeniser<'s, Token>;

// Parse material properties and stages

const DIFFUSEMAP: &str = "diffusemap";
const BUMPMAP: &str = "bumpmap";
const SPECULARMAP: &str = "specularmap";

// Parse a map expression. This must be either a file name, or a valid image modification
// expression e.g. addnormals(), heightmap() etc.
fn map_expression(tok: &mut Tokeniser) -> ParseResult<String> {
    // Parameter consuming helper(s)
    let take1map = |t: &mut Tokeniser, r: &str| -> ParseResult<String> {
        t.assert(Token::OpenParen)?;
        map_expression(t)?;
        t.assert(Token::CloseParen)?;
        Ok(String::from(r))
    };

    match tok.take()? {
        Token::AddNormals => {
            // addnormals(<map>, <map>)
            tok.assert(Token::OpenParen)?;
            map_expression(tok)?;
            tok.assert(Token::Comma)?;
            map_expression(tok)?;
            tok.assert(Token::CloseParen)?;
            Ok(String::from("<addnormals>"))
        }
        Token::HeightMap => {
            // heightmap(<map>, <float>)
            tok.assert(Token::OpenParen)?;
            map_expression(tok)?;
            tok.assert(Token::Comma)?;
            MathExpr::from_tokens(tok)?;
            tok.assert(Token::CloseParen)?;
            Ok(String::from("<heightmap>"))
        }
        Token::Scale => {
            // scale(<map>, <float> [, <float, ...])
            tok.assert(Token::OpenParen)?;
            map_expression(tok)?;
            tok.assert(Token::Comma)?;
            expr::RGBA::from_tokens_variable(tok, 0.0)?;
            tok.assert(Token::CloseParen)?;
            Ok(String::from("<scale>"))
        }
        Token::InvertColor => take1map(tok, "<invertColor>"),
        Token::InvertAlpha => take1map(tok, "<invertAlpha>"),
        Token::CameraLayout => take1map(tok, "<cameraLayout>"),
        Token::MakeAlpha => take1map(tok, "<makeAlpha>"),
        Token::MakeIntensity => take1map(tok, "<makeIntensity>"),
        Token::Name(name) => Ok(name.replace("\\", "/")),
        Token::QuotedString(value) => Ok(value),
        _ => Err(tok.unexpected()),
    }
}

impl Parseable for ImageExpr {
    // Construct an ImageExpr by parsing tokens
    fn from_tokens(mut tok: &mut Tokeniser) -> ParseResult<Self> {
        match tok.take()? {
            Token::AddNormals => {
                // addnormals(<map>, <map>)
                tok.assert(Token::OpenParen)?;
                let left = Self::from_tokens(&mut tok)?;
                tok.assert(Token::Comma)?;
                let right = Self::from_tokens(&mut tok)?;
                tok.assert(Token::CloseParen)?;
                Ok(Self::AddNormals(ImageExprRef::new(left), ImageExprRef::new(right)))
            }
            Token::HeightMap => {
                // heightmap(<map>, <float>)
                tok.assert(Token::OpenParen)?;
                let child = Self::from_tokens(&mut tok)?;
                tok.assert(Token::Comma)?;
                let mathexp = MathExpr::from_tokens(tok)?;
                tok.assert(Token::CloseParen)?;
                Ok(Self::HeightMap(ImageExprRef::new(child), mathexp))
            }
            Token::Scale => {
                // scale(<map>, <float> [, <float, ...])
                tok.assert(Token::OpenParen)?;
                let child = Self::from_tokens(&mut tok)?;
                tok.assert(Token::Comma)?;
                let scale = expr::RGBA::from_tokens_variable(tok, 0.0)?;
                tok.assert(Token::CloseParen)?;
                Ok(Self::Scale(ImageExprRef::new(child), scale))
            }
            Token::CameraLayout => {
                // cameraLayout(<map>)
                tok.assert(Token::OpenParen)?;
                let child = Self::from_tokens(&mut tok)?;
                tok.assert(Token::CloseParen)?;
                Ok(Self::CameraLayout(ImageExprRef::new(child)))
            }
            Token::Name(text) => Ok(Self::ImagePath(text.replace("\\", "/"))),
            _ => Err(tok.unexpected()),
        }
    }
}

impl Parseable for Stage {
    fn from_tokens(lex: &mut Tokeniser) -> ParseResult<Self> {
        // Every stage must start with an open brace
        lex.assert(Token::OpenBrace)?;
        // Contents follows
        Self::parse_contents(lex)
    }
}

impl Stage {
    // Create a Stage by parsing the token stream as the contents of a stage block. The
    // initial open brace is already assumed to be parsed.
    fn parse_contents(tok: &mut Tokeniser) -> ParseResult<Stage> {
        // Stage contents may appear in any order
        let mut stage = Stage::new();
        loop {
            match tok.take()? {
                Token::Blend => stage.blend_mode = BlendMode::from_tokens(tok)?,
                Token::Map => stage.image_path = map_expression(tok)?,
                Token::CubeMap => {
                    stage.image_path = map_expression(tok)?;
                    stage.image_layout = ImageLayout::Cube;
                }
                Token::CameraCubeMap => {
                    stage.image_path = map_expression(tok)?;
                    stage.image_layout = ImageLayout::CameraCube;
                }
                Token::VertexColor => stage.vertex_color_mode = VertexColorMode::Multiply,
                Token::InverseVertexColor => {
                    stage.vertex_color_mode = VertexColorMode::InverseMultiply;
                }
                Token::If => if_expression(tok)?,
                // General stage properties
                Token::Clamp => stage.clamp_mode = ClampMode::Clamp,
                Token::ZeroClamp => stage.clamp_mode = ClampMode::ZeroClamp,
                Token::AlphaZeroClamp => stage.alpha_zero_clamp = true,
                Token::Min => stage.min_value = tok.get_f32()?,
                Token::Max => stage.max_value = tok.get_f32()?,
                Token::LinearSteps => stage.linear_steps = tok.get_i32()?,
                // Image filtering and compression
                Token::Nearest => stage.image_filter = ImageFilter::Nearest,
                Token::Linear => stage.image_filter = ImageFilter::Linear,
                Token::NoPicMip => stage.no_pic_mip = true,
                Token::HighQuality => stage.image_compression = ImageCompress::High,
                Token::ForceHiQuality => stage.image_compression = ImageCompress::ForceHigh,
                // Alpha and channel masking
                Token::AlphaTest => drop(MathExpr::from_tokens(tok)?),
                Token::MaskAlpha => stage.channels = Channel::R | Channel::G | Channel::B,
                Token::MaskColor => stage.channels = enum_set!(Channel::A),
                Token::MaskDepth => stage.depth_write = false,
                // Colour modifications
                Token::Rgb => stage.rgba.set_intensity(MathExpr::from_tokens(tok)?),
                Token::Red => stage.rgba.r = MathExpr::from_tokens(tok)?,
                Token::Green => stage.rgba.g = MathExpr::from_tokens(tok)?,
                Token::Blue => stage.rgba.b = MathExpr::from_tokens(tok)?,
                Token::Alpha => stage.rgba.a = MathExpr::from_tokens(tok)?,
                Token::Color => stage.rgba = expr::RGBA::from_tokens(tok)?,
                Token::Colored => (),
                Token::TexGen => stage.texgen = Some(TexGen::from_tokens(tok)?),
                // Transforms
                Token::Scale => stage.scale = Vector2::from_tokens(tok)?,
                Token::CenterScale => stage.center_scale = Vector2::from_tokens(tok)?,
                Token::Translate | Token::Scroll => {
                    stage.translate = Vector2::from_tokens(tok)?
                }
                Token::Rotate => stage.rotate = MathExpr::from_tokens(tok)?,
                Token::Shear => stage.shear = Vector2::from_tokens(tok)?,
                // Shader programs
                Token::VertexProgram => stage.vertex_program = Some(tok.get_name()?),
                Token::FragmentProgram => stage.fragment_program = Some(tok.get_name()?),
                Token::Program => {
                    let name = tok.get_name()?;
                    stage.vertex_program = Some(name.clone());
                    stage.fragment_program = Some(name);
                }
                Token::VertexParm => match tok.take()? {
                    Token::Number(index) => {
                        // No comma between index and first value
                        stage.vertex_parms.insert(index as i32, Vector4::from_tokens(tok)?);
                    }
                    _ => return Err(tok.unexpected()),
                },
                Token::FragmentMap => match tok.take()? {
                    Token::Number(index) => {
                        let frag_map = FragmentMap::from_tokens(tok)?;
                        stage.fragment_maps.insert(index as i32, frag_map);
                    }
                    _ => return Err(tok.unexpected()),
                },
                // Render maps
                Token::MirrorRenderMap => {
                    stage.mirror_render_map = Some(BufferSize::from_tokens(tok)?);
                }
                Token::RemoteRenderMap => {
                    stage.remote_render_map = Some(BufferSize::from_tokens(tok)?)
                }
                Token::VideoMap => stage.video_map = Some(VideoMap::from_tokens(tok)?),
                // End of stage
                Token::CloseBrace => break,
                _ => return Err(tok.unexpected()),
            }
        }
        Ok(stage)
    }
}

fn if_expression(tokeniser: &mut Tokeniser) -> ParseResult<()> {
    tokeniser.assert(Token::OpenParen)?;

    // For now we don't interpret conditional expressions, just swallow and discard
    let mut depth = 1;
    loop {
        match tokeniser.take()? {
            Token::OpenParen => depth += 1,
            Token::CloseParen => depth -= 1,
            _ => continue,
        }
        if depth == 0 {
            break;
        }
    }

    Ok(())
}

impl RGBA {
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<RGBA> {
        let r = tok.get_f32()?;
        let g = tok.get_f32()?;
        let b = tok.get_f32()?;
        let a = tok.get_f32()?;

        Ok(RGBA { r, g, b, a })
    }
}

impl MathExpr {
    /// Attempt to construct a MathExpr from a token stream
    fn from_tokens(tokeniser: &mut Tokeniser) -> ParseResult<MathExpr> {
        // First token must be a number (which may itself be preceded by a "-" or "+"
        // token), a variable, or an open parenthesis introducing a sub-expression
        let expr = match tokeniser.take()? {
            Token::Plus => MathExpr::constant(tokeniser.get_f64()?),
            Token::Minus => MathExpr::constant(tokeniser.get_f64()? * -1.0),
            Token::Number(value) => MathExpr::constant(value),
            Token::Parm(name) => MathExpr::input(name),
            Token::OpenParen => {
                // Contents must be followed by a closing parenthesis
                let expr = Self::from_tokens(tokeniser)?;
                tokeniser.assert(Token::CloseParen)?;
                // Return parsed sub-expression out of the match block
                expr
            }
            Token::Name(table) => {
                // If the first token is a table name, it must immediately be followed by
                // another expression in square brackets
                tokeniser.assert(Token::OpenBracket)?;
                let time_expr = Self::from_tokens(tokeniser)?;
                tokeniser.assert(Token::CloseBracket)?;
                // Construct the MathExpr for table lookup
                MathExpr::table(table, time_expr)
            }
            _ => return Err(tokeniser.unexpected()),
        };

        // A math operation like * or + introduces a new expression, otherwise we just return
        // the already-parsed expression.
        Ok(match tokeniser.try_take() {
            Some(Token::Times) => MathExpr::multiply(expr, Self::from_tokens(tokeniser)?),
            Some(Token::Plus) => MathExpr::add(expr, Self::from_tokens(tokeniser)?),
            Some(Token::Minus) => MathExpr::subtract(expr, Self::from_tokens(tokeniser)?),
            Some(Token::Divide) => MathExpr::divide(expr, Self::from_tokens(tokeniser)?),
            Some(Token::Or) => MathExpr::or(expr, Self::from_tokens(tokeniser)?),
            Some(other) => {
                tokeniser.push(other);
                expr
            }
            None => expr,
        })
    }
}

impl Tokeniser<'_> {
    /// Attempt to parse and return the next token as a name
    fn get_name(&mut self) -> ParseResult<String> {
        if let Token::Name(n) = self.take()? {
            Ok(n)
        } else {
            Err(self.unexpected())
        }
    }

    /// Attempt to parse and return the next token as a f64
    fn get_f64(&mut self) -> ParseResult<f64> {
        // Handle an initial negative sign
        let sign = if let Some(Token::Minus) = self.peek() {
            self.discard();
            -1.0
        } else {
            1.0
        };

        // Require a number
        if let Token::Number(v) = self.take()? {
            Ok(sign * v)
        } else {
            Err(self.unexpected())
        }
    }

    /// Attempt to parse and return the next token as a f32
    fn get_f32(&mut self) -> ParseResult<f32> {
        self.get_f64().map(|f| f as f32)
    }

    /// Attempt to parse and return the next token as an i32. If the number is not an
    /// integer, this will return an error.
    fn get_i32(&mut self) -> ParseResult<i32> {
        let number = self.get_f64()?;
        if number.fract() == 0.0 {
            Ok(number as i32)
        } else {
            Err(self.unexpected())
        }
    }
}

fn deform_expression(tokeniser: &mut Tokeniser) -> ParseResult<Deform> {
    match tokeniser.take()? {
        Token::Sprite => return Ok(Deform::Sprite),
        Token::Tube => return Ok(Deform::Tube),
        Token::Eyeball => return Ok(Deform::Eyeball),
        Token::Flare => {
            return Ok(Deform::Flare(tokeniser.get_f32()?));
        }
        Token::Particle => {
            return Ok(Deform::Particle(tokeniser.get_name()?));
        }
        Token::Particle2 => {
            return Ok(Deform::Particle2(tokeniser.get_name()?));
        }
        Token::Turbulent => {
            let table = tokeniser.get_name()?;
            let range = MathExpr::from_tokens(tokeniser)?;
            let offset = MathExpr::from_tokens(tokeniser)?;
            let domain = MathExpr::from_tokens(tokeniser)?;
            return Ok(Deform::Turbulent(
                table,
                range.try_value().unwrap_or_default(),
                offset.try_value().unwrap_or_default(),
                domain.try_value().unwrap_or_default(),
            ));
        }
        _ => return Err(tokeniser.unexpected()),
    }
}

impl BlendMode {
    /// Attempt to construct a BlendMode by interpreting the given token
    fn from_token(tok: &Token) -> ParseResult<Self> {
        match tok {
            Token::DiffuseMap => Ok(BlendMode::Diffuse),
            Token::BumpMap => Ok(BlendMode::Bump),
            Token::SpecularMap => Ok(BlendMode::Specular),
            Token::ParallaxMap => Ok(BlendMode::Parallax),
            Token::Add => Ok(BlendMode::Add),
            Token::Filter | Token::Modulate => Ok(BlendMode::Multiply),
            Token::Blend => Ok(BlendMode::Blend),
            other => Err(ParseError::UnexpectedToken(format!("{:?}", other), None)),
        }
    }

    /// Construct a BlendMode by reading a token stream
    fn from_tokens(lex: &mut Tokeniser) -> ParseResult<Self> {
        // Try single tokens first
        let first_tok = lex.take()?;
        if let Ok(result) = Self::from_token(&first_tok) {
            return Ok(result);
        }

        // Not a single-token blend mode, therefore the first token must be a GlCustomBlend
        // and the next token must be a comma.
        let first_mode = match first_tok {
            Token::GlCustomBlend(mode) => mode,
            whatever => return Err(ParseError::from_tok(&whatever)),
        };
        lex.assert(Token::Comma)?;

        // Final token must be another custom blend mode
        let second_mode = match lex.take()? {
            Token::GlCustomBlend(mode) => mode,
            whatever => return Err(ParseError::from_tok(&whatever)),
        };

        Ok(BlendMode::Custom(first_mode, second_mode))
    }
}

impl SortPosition {
    /// Construct a SortPosition by interpreting a given token
    fn from_token(token: &Token) -> ParseResult<Self> {
        match token {
            Token::Decal => Ok(SortPosition::Decal),
            Token::Far => Ok(SortPosition::Far),
            Token::Medium => Ok(SortPosition::Medium),
            Token::Close => Ok(SortPosition::Close),
            Token::Nearest => Ok(SortPosition::Nearest),
            Token::PostProcess => Ok(SortPosition::PostProcess),
            Token::Last => Ok(SortPosition::Last),
            Token::Number(value) => Ok(SortPosition::Custom(*value as i32)),
            other => Err(ParseError::from_tok(other)),
        }
    }
}

impl Table {
    /// Construct a Table from a token stream
    fn from_tokens(lex: &mut Tokeniser) -> ParseResult<Self> {
        // After "table" must be the string name
        lex.assert(Token::Table)?;
        let name = lex.get_name()?;

        // Initial open brace
        lex.assert(Token::OpenBrace)?;

        // Optional keywords
        let mut snap = false;
        let mut clamp = false;
        while let Ok(t) = lex.take() {
            match t {
                Token::Clamp => clamp = true,
                Token::Snap => snap = true,
                Token::OpenBrace => break, // start of values block
                _ => return Err(lex.unexpected()),
            }
        }

        // Parse as many values as are in the table
        let mut values = Vec::new();
        while let Ok(t) = lex.take() {
            match t {
                Token::Plus => values.push(lex.get_f64()?),
                Token::Minus => values.push(-1.0 * lex.get_f64()?),
                Token::Number(value) => values.push(value),
                Token::Comma => {}          // skip
                Token::CloseBrace => break, // end of values
                _ => return Err(lex.unexpected()),
            }
        }

        // Final close brace for the whole decl
        lex.assert(Token::CloseBrace)?;

        Ok(Self { name, values, snap, clamp })
    }
}

impl TexGen {
    /// Construct a TexGen by parsing tokens
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<TexGen> {
        match tok.take()? {
            Token::Reflect => Ok(TexGen::Reflect),
            Token::SkyBox => Ok(TexGen::Skybox),
            Token::Normal => Ok(TexGen::Normal),
            Token::WobbleSky => {
                let x = MathExpr::from_tokens(tok)?;
                let y = MathExpr::from_tokens(tok)?;
                let z = MathExpr::from_tokens(tok)?;
                Ok(TexGen::WobbleSky(x, y, z))
            }
            _ => Err(tok.unexpected()),
        }
    }
}

impl DecalInfo {
    /// Construct a DecalInfo by parsing tokens
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Self> {
        // Mandatory time information
        let stay_secs = tok.get_f32()?;
        let fade_secs = tok.get_f32()?;

        // Optional initial and final colours
        let start_rgba;
        if let Some(Token::OpenParen) = tok.peek() {
            tok.discard();
            start_rgba = RGBA::from_tokens(tok)?;
            tok.assert(Token::CloseParen)?;
        } else {
            start_rgba = RGBA::white();
        }

        let end_rgba;
        if let Some(Token::OpenParen) = tok.peek() {
            tok.discard();
            end_rgba = RGBA::from_tokens(tok)?;
            tok.assert(Token::CloseParen)?;
        } else {
            end_rgba = RGBA::black();
        }

        Ok(Self { stay_secs, fade_secs, start_rgba, end_rgba })
    }
}

impl BufferSize {
    /// Construct a BufferSize by parsing tokens
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Self> {
        let width = tok.get_i32()?;
        let height = tok.get_i32()?;

        Ok(Self { width, height })
    }
}

impl VideoMap {
    /// Construct a VideoMap by parsing tokens
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Self> {
        let mut is_looping = false;
        loop {
            match tok.take()? {
                Token::Loop => {
                    if is_looping {
                        // Only one "loop" allowed
                        return Err(tok.unexpected());
                    } else {
                        is_looping = true;
                    }
                }
                Token::Name(n) => return Ok(Self { video_path: n, is_looping }),
                _ => return Err(tok.unexpected()),
            }
        }
    }
}

impl RenderBump {
    /// Construct a RenderBump by parsing tokens
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<RenderBump> {
        let mut result = RenderBump::new();
        loop {
            match tok.take()? {
                Token::RenderBumpSize => {
                    result.width = tok.get_f64()? as i32;
                    result.height = tok.get_f64()? as i32;
                }
                Token::RenderBumpAA => {
                    result.aa_level = tok.get_f64()? as i32;
                }
                Token::RenderBumpTrace => {
                    result.trace = tok.get_f64()?;
                }
                Token::RenderBumpColorMap => { /* undocumented */ }
                Token::Name(n) => {
                    if result.image.is_empty() {
                        result.image = n;
                    } else {
                        result.model = n;
                        break;
                    }
                }
                _ => return Err(tok.unexpected()),
            }
        }
        Ok(result)
    }
}

impl FragmentMap {
    /// Construct a FragmentMap by parsing tokens
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Self> {
        // Possible map options
        let mut name: Option<String> = None;
        let mut filter: Option<ImageFilter> = None;
        let mut compress: Option<ImageCompress> = None;
        let mut clamp: Option<ClampMode> = None;

        // First token might be an option or an image map
        match tok.take()? {
            Token::ForceHiQuality => compress = Some(ImageCompress::ForceHigh),
            Token::HighQuality => compress = Some(ImageCompress::High),
            Token::Clamp => clamp = Some(ClampMode::Clamp),
            Token::Linear => filter = Some(ImageFilter::Linear),
            Token::Name(n) => name = Some(n),
            _ => return Err(tok.unexpected()),
        }

        // If name isn't set, first token must have been an option, so parse the next token
        // as the name.
        if name.is_none() {
            name = Some(tok.get_name()?);
        }

        // Name must be set by now otherwise we would have returned early
        Ok(Self {
            image_path: name.unwrap(),
            image_filter: filter.unwrap_or_default(),
            image_compression: compress.unwrap_or_default(),
            clamp_mode: clamp.unwrap_or_default(),
        })
    }
}

impl expr::RGBA {
    /// Construct an RGBA expression from a token stream.
    ///
    /// All four components must be present and separated by commas.
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Self> {
        let r = MathExpr::from_tokens(tok)?;
        tok.assert(Token::Comma)?;
        let g = MathExpr::from_tokens(tok)?;
        tok.assert(Token::Comma)?;
        let b = MathExpr::from_tokens(tok)?;
        tok.assert(Token::Comma)?;
        let a = MathExpr::from_tokens(tok)?;

        Ok(expr::RGBA { r, g, b, a })
    }

    /// Construct an RGBA expression from a variable token stream.
    ///
    /// At least one component must be present. Missing components will be filled in with
    /// the given default value.
    fn from_tokens_variable(tok: &mut Tokeniser, default: f64) -> ParseResult<Self> {
        // Mandatory first value
        let r = MathExpr::from_tokens(tok)?;
        let g: MathExpr;
        let b: MathExpr;
        let a: MathExpr;

        // Check for each remaining value
        let mut try_next = || -> ParseResult<Option<MathExpr>> {
            match tok.peek() {
                Some(Token::Comma) => {
                    tok.discard();
                    Ok(Some(MathExpr::from_tokens(tok)?))
                }
                _ => Ok(None),
            }
        };
        g = try_next()?.unwrap_or_else(|| MathExpr::constant(default));
        b = try_next()?.unwrap_or_else(|| MathExpr::constant(default));
        a = try_next()?.unwrap_or_else(|| MathExpr::constant(default));

        Ok(Self { r, g, b, a })
    }

    /// Populate RGB from a single expression and leave alpha untouched
    fn set_intensity(&mut self, intensity: MathExpr) {
        self.r = intensity.clone();
        self.g = intensity.clone();
        self.b = intensity;
    }
}

impl Vector2 {
    /// Construct a Vector2 from a token stream
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Vector2> {
        let x = MathExpr::from_tokens(tok)?;
        tok.assert(Token::Comma)?;
        let y = MathExpr::from_tokens(tok)?;

        Ok(Vector2 { x, y })
    }
}

impl Vector4 {
    /// Construct a Vector4 from a token stream
    fn from_tokens(tok: &mut Tokeniser) -> ParseResult<Vector4> {
        // X value is compulsory
        let x = MathExpr::from_tokens(tok)?;

        let zero: MathExpr = MathExpr::constant(0.0);
        let one: MathExpr = MathExpr::constant(1.0);

        let mut result = Vector4 { x, y: zero.clone(), z: zero, w: one };

        let getnext = |tok: &mut Tokeniser| {
            if let Some(Token::Comma) = tok.peek() {
                tok.discard();
                if let Ok(e) = MathExpr::from_tokens(tok) {
                    return Some(e);
                }
            }
            None
        };

        // Optional Y component
        if let Some(yexpr) = getnext(tok) {
            result.y = yexpr;
        } else {
            return Ok(result);
        }

        // Optional Z component
        if let Some(zexpr) = getnext(tok) {
            result.z = zexpr;
        } else {
            return Ok(result);
        }

        // Optional W component
        if let Some(wexpr) = getnext(tok) {
            result.w = wexpr;
        }

        return Ok(result);
    }
}

impl Material {
    // Parse optional decls inside a material block. Name and opening brace are already parsed.
    fn populate(&mut self, lex: &mut Tokeniser) -> ParseResult<()> {
        loop {
            match lex.take()? {
                // Surface types
                Token::Flesh => self.surface_type = Some(SurfaceType::Flesh),
                Token::Metal => self.surface_type = Some(SurfaceType::Metal),
                Token::Wood => self.surface_type = Some(SurfaceType::Wood),
                Token::Stone => self.surface_type = Some(SurfaceType::Stone),
                Token::Glass => self.surface_type = Some(SurfaceType::Glass),
                Token::Gravel => self.surface_type = Some(SurfaceType::Gravel),
                Token::Water => self.surface_type = Some(SurfaceType::Water),
                Token::Blood => self.surface_type = Some(SurfaceType::Blood),
                Token::Liquid => self.surface_type = Some(SurfaceType::Liquid),
                Token::Plastic => self.surface_type = Some(SurfaceType::Plastic),
                Token::SurfType15 => self.surface_type = Some(SurfaceType::SurfType15),
                Token::Ricochet => self.surface_type = Some(SurfaceType::Ricochet),
                Token::Ladder => self.is_climbable = true,

                // Material flags
                Token::NoShadows => self.no_shadows = true,
                Token::NoSelfShadow => self.no_self_shadow = true,
                Token::ForceShadows => self.force_shadows = true,
                Token::TwoSided => self.two_sided = true,
                Token::ForceOpaque => self.force_opaque = true,
                Token::ForceOverlays => self.force_overlays = true,
                Token::NoOverlays => self.no_overlays = true,
                Token::NonSolid => self.non_solid = true,
                Token::Solid => self.non_solid = false,
                Token::Translucent => self.translucent = true,
                Token::NoImpact => self.no_impact = true,
                Token::NoFog => self.no_fog = true,
                Token::NoPortalFog => self.no_portal_fog = true,
                Token::Sort => {
                    self.sort_position = Some(SortPosition::from_token(&lex.take()?)?)
                }
                Token::UnsmoothedTangents => self.unsmoothed_tangents = true,
                Token::Discrete => self.discrete = true,
                Token::NoFragment => self.no_fragment = true,
                Token::Collision => self.is_collision = true,
                Token::PlayerClip => self.player_clip = true,
                Token::Slick => self.slick = true,
                Token::DecalMacro => {
                    self.no_shadows = true;
                    self.discrete = true;
                    self.non_solid = true;
                    self.sort_position = Some(SortPosition::Decal);
                }
                Token::TwoSidedDecalMacro => {
                    self.no_shadows = true;
                    self.no_self_shadow = true;
                    self.discrete = true;
                    self.no_impact = true;
                    self.non_solid = true;
                    self.two_sided = true;
                    self.translucent = true;
                    self.sort_position = Some(SortPosition::Decal);
                }
                Token::DecalInfo => self.decal_info = Some(DecalInfo::from_tokens(lex)?),
                Token::ParticleMacro => {
                    self.discrete = true;
                    self.no_impact = true;
                    self.non_solid = true;
                    self.no_self_shadow = true;
                    self.no_shadows = true;
                    self.translucent = true;
                }
                Token::GlassMacro => {
                    self.translucent = true;
                    self.two_sided = true;
                    self.no_shadows = true;
                    self.no_self_shadow = true;
                }

                // Other global properties
                Token::Deform => self.deform = Some(deform_expression(lex)?),
                Token::Spectrum => self.spectrum = Some(lex.get_i32()?),
                Token::Clamp => self.clamp_mode = ClampMode::Clamp,
                Token::ZeroClamp => self.clamp_mode = ClampMode::ZeroClamp,
                Token::PolygonOffset => self.polygon_offset = lex.get_f64()?,
                Token::GuiSurf => self.gui_surf = Some(lex.get_name()?),
                Token::AreaPortal => self.is_area_portal = true,
                Token::NoCarve => self.no_carve = true,
                Token::AasSolid => self.aas_solid = true,
                Token::AasObstacle => self.aas_obstacle = true,
                Token::FogAlpha => self.fog_alpha = lex.get_f32()?,

                // Light properties
                Token::AmbientLight => self.light_type = Some(LightType::Ambient),
                Token::FogLight => self.light_type = Some(LightType::Fog),
                Token::BlendLight => self.light_type = Some(LightType::Blend),
                Token::LightFalloff => self.light_falloff = Some(map_expression(lex)?),

                // Component images
                mode_tok @ (Token::DiffuseMap | Token::BumpMap | Token::SpecularMap) => {
                    let mut stage = Stage::with_blend_mode(BlendMode::from_token(&mode_tok)?);
                    stage.image_path = map_expression(lex)?;
                    self.stages.push(stage);
                }
                Token::EditorImage => self.editor_image = Some(map_expression(lex)?),

                // Frob stages
                Token::FrobStageNone => self.frob_stage = FrobStage::None,
                Token::FrobStageDiffuse => {
                    self.frob_stage = FrobStage::Diffuse(lex.get_f64()?, lex.get_f64()?)
                }
                Token::FrobStageTexture => {
                    let tex = lex.get_name()?;
                    self.frob_stage = FrobStage::ImageMap(tex, lex.get_f64()?, lex.get_f64()?)
                }

                // Block syntax
                Token::OpenBrace => self.stages.push(Stage::parse_contents(lex)?),
                Token::CloseBrace => break,

                // Material description
                Token::Description => match lex.take()? {
                    // Most descriptions should be quoted
                    Token::QuotedString(value) => self.description = value,
                    // Allow unquoted single words
                    Token::Name(value) => self.description = value,
                    // Some unquoted single words might be recognised as another token (e.g. "stone")
                    _ => self.description = lex.slice().to_owned(),
                },

                // renderBump parameters
                Token::RenderBump => self.render_bump = Some(RenderBump::from_tokens(lex)?),

                // Anything unrecognised
                _ => return Err(lex.unexpected()),
            }
        }

        Ok(())
    }
}

impl Parseable for Material {
    fn from_tokens(lex: &mut Tokeniser) -> ParseResult<Self> {
        // Material must start with a name
        let name_tok = lex.take()?;
        if let Token::Name(name) = name_tok {
            // Confirm we have the expected open brace before allocating the Material
            lex.assert(Token::OpenBrace)?;
            let mut mat = Material::new(name);

            // Parse each optional decl inside the material body
            mat.populate(lex)?;

            return Ok(mat);
        }

        Err(lex.unexpected())
    }
}

impl ParsedMaterials {
    /// Parse as many materials as possible from the given token stream
    pub fn from_tokens(lex: &mut Tokeniser) -> Self {
        let mut result = Self::new();
        loop {
            // The "table" token introduces a table
            if let Some(Token::Table) = lex.peek() {
                match Table::from_tokens(lex) {
                    Ok(table) => result.tables.push(table),
                    Err(e) => {
                        result.last_error = Some(e);
                        break;
                    }
                }
            } else {
                // Otherwise try to parse a named material
                match Material::from_tokens(lex) {
                    Ok(material) => result.materials.push(material),
                    Err(ParseError::NoMoreTokens) => break,
                    Err(unexpected) => {
                        result.last_error = Some(unexpected);
                        break;
                    }
                }
            }
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::mtr::expr::ValueProvider;
    use std::cell::Cell;

    const SIMPLE_MTR: &str = "textures/orbweaver/redwood_boards\
                                   {
                                       wood
                                       diffusemap textures/orbweaver/redwood_boards_d
                                       bumpmap textures/orbweaver/redwood_boards_local
                                   }";

    const COMPLEX_MTR: &str = "textures/orbweaver/complex_material0
                                    {
                                        stone
                                        sort nearest
                                        {
                                            blend diffusemap
                                            map textures/orbweaver/greenplaster_d
                                            vertexColor
                                        }
                                        {
                                            blend diffusemap
                                            map textures/orbweaver/redplaster_d
                                            inverseVertexColor
                                        }
                                        bumpmap textures/orbweaver/greenplaster_local
                                    }";

    const MULTI_MTRS: &str = "sk/pathblocks3
                                   {
                                       stone
                                       diffusemap models/orbweaver/pathblocks3_d
                                       bumpmap models/orbweaver/pathblocks3_local
                                   }

                                   textures/orbweaver/green_old_plaster
                                   {
                                       stone
                                       {
                                           blend diffusemap
                                           map textures/orbweaver/greenplaster_d
                                       }
                                       bumpmap textures/orbweaver/greenplaster_local
                                   }

                                   sk/third_texture {
                                       wood
                                   }";

    #[test]
    fn tokeniser_can_peek() {
        let source = "name { first second }";
        let mut tokeniser = Tokeniser::from_str(source);

        assert_eq!(tokeniser.take(), Ok(Token::Name(String::from("name"))));
        assert_eq!(tokeniser.take(), Ok(Token::OpenBrace));

        // Peek the next token
        assert_eq!(tokeniser.peek(), Some(Token::Name(String::from("first"))));

        // Multiple peeks return the same token
        assert_eq!(tokeniser.peek(), Some(Token::Name(String::from("first"))));
        assert_eq!(tokeniser.peek(), Some(Token::Name(String::from("first"))));

        // Peek does not consume the token, so it should still be returned
        assert_eq!(tokeniser.take(), Ok(Token::Name(String::from("first"))));
        assert_eq!(tokeniser.take(), Ok(Token::Name(String::from("second"))));
    }

    #[test]
    fn parse_simple_mtr_name() {
        let test_name = "textures/orbweaver/redwood_boards".to_owned();

        let mut lex = Token::lexer(&test_name);
        assert_eq!(lex.next(), Some(Token::Name(test_name)));
    }

    #[test]
    fn parse_name_with_backslashes() {
        let name = "textures\\particles\\lanternflare".to_owned();

        let mut tok = Token::lexer(&name);
        assert_eq!(tok.next(), Some(Token::Name(name)));
    }

    #[test]
    fn parse_stage_block() {
        let parse = |source| {
            let mut lex = Tokeniser::from_str(source);
            Stage::from_tokens(&mut lex)
        };

        assert!(parse("not a block").is_err());
        assert!(parse("{{{}").is_err());
        assert_eq!(
            parse("{ blend diffusemap }").map(|s| s.blend_mode),
            Ok(BlendMode::Diffuse)
        );

        let block = "{
                blend bumpmap
                map textures/path/to/bump_map_d.tga
            }";
        let stage = parse(block).unwrap();
        assert_eq!(stage.blend_mode, BlendMode::Bump);
        assert_eq!(stage.image_path, "textures/path/to/bump_map_d.tga");
    }

    #[test]
    fn parse_parallaxmap() {
        let text = "{
            blend parallaxmap
            map textures/darkmod/foliage/leaves_fallen_fading_disp
            max .005
            min -.1
            linearSteps 20
        }";

        let stage = Stage::from_str(text).unwrap();
        assert_eq!(stage.blend_mode, BlendMode::Parallax);
        assert_eq!(stage.min_value, -0.1);
        assert_eq!(stage.max_value, 0.005);
        assert_eq!(stage.linear_steps, 20);
        assert_eq!(stage.image_path, "textures/darkmod/foliage/leaves_fallen_fading_disp");
    }

    #[test]
    fn parse_full_material() {
        assert!(Material::from_str("not a material").is_err());

        // Simple material with single-line stages
        let simple = Material::from_str(SIMPLE_MTR).unwrap();
        assert_eq!(simple.name, "textures/orbweaver/redwood_boards");
        assert_eq!(simple.surface_type, Some(SurfaceType::Wood));
        assert_eq!(simple.stages.len(), 2);
        assert_eq!(simple.stages[0].blend_mode, BlendMode::Diffuse);
        assert_eq!(simple.stages[0].image_path, "textures/orbweaver/redwood_boards_d");
        assert_eq!(simple.stages[1].blend_mode, BlendMode::Bump);
        assert_eq!(simple.stages[1].image_path, "textures/orbweaver/redwood_boards_local");

        // Multi-line stages
        let multi = Material::from_str(COMPLEX_MTR).unwrap();
        assert_eq!(multi.name, "textures/orbweaver/complex_material0");
        assert_eq!(multi.surface_type, Some(SurfaceType::Stone));
        assert_eq!(multi.sort_position, Some(SortPosition::Nearest));
        assert_eq!(multi.stages[0].blend_mode, BlendMode::Diffuse);
        assert_eq!(multi.stages[0].image_path, "textures/orbweaver/greenplaster_d");
        assert_eq!(multi.stages[0].vertex_color_mode, VertexColorMode::Multiply);
        assert_eq!(multi.stages[1].blend_mode, BlendMode::Diffuse);
        assert_eq!(multi.stages[1].image_path, "textures/orbweaver/redplaster_d");
        assert_eq!(multi.stages[1].vertex_color_mode, VertexColorMode::InverseMultiply);
        assert_eq!(multi.stages[2].blend_mode, BlendMode::Bump);
        assert_eq!(multi.stages[2].image_path, "textures/orbweaver/greenplaster_local");
    }

    #[test]
    fn parse_multi_materials() {
        let mut lex = Tokeniser::from_str(MULTI_MTRS);
        let result = ParsedMaterials::from_tokens(&mut lex);
        assert_eq!(result.materials.len(), 3);
        assert_eq!(result.materials[0].name, "sk/pathblocks3");
        assert_eq!(result.materials[1].name, "textures/orbweaver/green_old_plaster");
        assert_eq!(result.materials[2].name, "sk/third_texture");
        assert!(result.last_error.is_none());
    }

    #[test]
    fn parse_multi_materials_with_err() {
        let multi_with_err = "textures/orbweaver/redwood_boards\
                                        {
                                            diffusemap textures/orbweaver/redwood_boards_d
                                            bumpmap textures/orbweaver/redwood_boards_local
                                        }

                                        textures/second {
                                            wood
                                            diffusemap another/diffuse
                                        }

                                        wrong blah";
        let mut lex = Tokeniser::from_str(multi_with_err);
        let result = ParsedMaterials::from_tokens(&mut lex);

        // First two should be parsed
        assert_eq!(result.materials.len(), 2);

        // Last error should be stored. Note that "wrong" is a valid texture name so it's not
        // until "blah" is seen that the error is detected.
        let last_err = result.last_error.unwrap();
        assert_eq!(last_err, ParseError::UnexpectedToken("blah".to_owned(), Some(528..532)));
    }

    #[test]
    fn logos_handle_error() {
        // Error inside the decl should result in a ParseError
        let error_decl = "textures/darkmod/failed_mat {
                diffusemap texture/blah/bleh
                this is wrong   }";
        let err = Material::from_str(error_decl).unwrap_err();
        match err {
            ParseError::NoMoreTokens => panic!("Expected UnexpectedToken"),
            ParseError::UnexpectedToken(tok, span) => {
                assert_eq!(tok, String::from("this"));

                // Error should contain the input range
                let src_span = &error_decl[span.unwrap()];
                assert_eq!(src_span, "this");
            }
        }

        // Gibberish after the decl shouldn't matter
        let error_after = "textures/darkmod/proper_mat
            {
               diffusemap texture/blah/bleh }
                this is wrong   }";
        let result = Material::from_str(error_after).unwrap();
        assert_eq!(result.name, "textures/darkmod/proper_mat");
        assert_eq!(result.stages.len(), 1);
    }

    #[test]
    fn ignore_cpp_comments() {
        let nexttok = |source| {
            let mut lex = Token::lexer(source);
            lex.next()
        };

        assert_eq!(nexttok("// C++ comment"), None);
        assert_eq!(
            nexttok("// C++ comment\ntextures/darkmod/name"),
            Some(Token::Name("textures/darkmod/name".to_owned()))
        );
    }

    #[test]
    fn ignore_c_comments() {
        const SRC: &str = "/*
                           Multi-line
                             comment
                   */

                   textures/darkmod/glass/clear_warp
                   {
                       qer_editorimage  textures/darkmod/glass/clear_warp_ed
                   }";

        let mat = Material::from_str(SRC).unwrap();
        assert_eq!(mat.name, "textures/darkmod/glass/clear_warp");
        assert_eq!(
            mat.editor_image,
            Some(String::from("textures/darkmod/glass/clear_warp_ed"))
        );
    }

    #[test]
    fn parse_surface_types() {
        const ST15: &str = "textures/darkmod/fabric/ship_sails
                                {
                                    surftype15
                                    twosided
                                    qer_editorimage  textures/darkmod/fabric/cloth_plain_creased_brown_light_ed
                                    diffusemap      textures/darkmod/fabric/cloth_plain_creased_brown_light
                                }";

        assert_eq!(
            Material::from_str(ST15).unwrap().surface_type,
            Some(SurfaceType::SurfType15)
        );

        // Metal
        let mat = Material::from_str("test { metal }").unwrap();
        assert_eq!(mat.surface_type, Some(SurfaceType::Metal));

        // Flesh
        let mat = Material::from_str("test/flesh { flesh   } ").unwrap();
        assert_eq!(mat.surface_type, Some(SurfaceType::Flesh));
    }

    #[test]
    fn parse_decal_macro() {
        const SRC: &str = "textures/darkmod/decals/airship/cannonball_impact
                           {
                               qer_editorimage textures/darkmod/decals/cracks/cannonball_impact
                               DECAL_MACRO
                           }";

        let mat = Material::from_str(SRC).unwrap();
        assert_eq!(mat.sort_position, Some(SortPosition::Decal));
        assert_eq!(mat.no_shadows, true);
        assert_eq!(mat.discrete, true);

        let twosided = "textures/decals/stain01b {
	        TWOSIDED_DECAL_MACRO
            qer_editorimage textures/decals/stain01b.tga
            {
                blend   gl_dst_color, gl_zero
                map textures/decals/stain01b.tga
            }
        }";
        let mat = Material::from_str(&twosided).unwrap();
        assert_eq!(mat.two_sided, true);
        assert_eq!(mat.non_solid, true);
        assert_eq!(mat.translucent, true);
    }

    #[test]
    fn parse_material_flags() {
        const ZOMBIE: &str = "models/md5/monsters/undead/zombie/zombie_rotten
                              {
                                noSelfShadow
                                unsmoothedTangents
                                forceoverlays
                                slick
                                playerclip
                                diffusemap    models/md5/chars/undead/tdm_zombie_rotten
                                bumpmap          models/md5/chars/undead/tdm_zombie_rotten_local

                                specularmap models/md5/chars/undead/tdm_zombie_rotten_s
                              }";

        let zombie = Material::from_str(ZOMBIE).unwrap();
        assert_eq!(zombie.no_self_shadow, true);
        assert_eq!(zombie.force_overlays, true);
        assert_eq!(zombie.force_opaque, false);
        assert_eq!(zombie.force_overlays, true);
        assert_eq!(zombie.no_fragment, false);
        assert_eq!(zombie.no_overlays, false);
        assert_eq!(zombie.slick, true);
        assert_eq!(zombie.no_impact, false);
        assert_eq!(zombie.player_clip, true);
        assert_eq!(zombie.is_climbable, false);
        assert_eq!(zombie.no_fog, false);

        const SQSKY: &str = "textures/darkmod/nature/skybox/sqsky
                             {
                                noFragment
                                noimpact
                                nooverlays
                                forceOpaque
                                noShadows
                                noFog
                                ladder
                             }";

        let sqsky = Material::from_str(SQSKY).unwrap();
        assert_eq!(sqsky.force_opaque, true);
        assert_eq!(sqsky.force_overlays, false);
        assert_eq!(sqsky.no_fragment, true);
        assert_eq!(sqsky.no_overlays, true);
        assert_eq!(sqsky.slick, false);
        assert_eq!(sqsky.no_impact, true);
        assert_eq!(sqsky.player_clip, false);
        assert_eq!(sqsky.is_climbable, true);
        assert_eq!(sqsky.no_fog, true);
    }

    #[test]
    fn parse_conditional_expression() {
        // Valid expression
        let mut lex = Tokeniser::from_str("( parm7 > 3 )");
        assert!(if_expression(&mut lex).is_ok());

        // Valid expression without spaces
        let mut lex = Tokeniser::from_str("(parm11 == 31.25)");
        assert!(if_expression(&mut lex).is_ok());

        // Invalid expression
        let mut lex = Tokeniser::from_str("parm11 !=+31.25;;");
        assert!(if_expression(&mut lex).is_err());

        // Expression with nesting
        let mut tok = Tokeniser::from_str("( (parm1 + 2) * 3 )");
        assert!(if_expression(&mut tok).is_ok());
    }

    #[test]
    fn parse_mtr_with_conditional() {
        let source = "textures/darkmod/conditional_test {
            diffusemap test/diffuse
            {
                if ( parm 11 > 0 )
                blend bumpmap
                map test/bump/map
            }
        }";
        let mut lex = Tokeniser::from_str(source);

        let mat = Material::from_tokens(&mut lex).unwrap();
        assert_eq!(mat.stages.len(), 2);
        assert_eq!(mat.stages[0].image_path, "test/diffuse");
        assert_eq!(mat.stages[1].image_path, "test/bump/map");
    }

    #[test]
    fn parse_complex_conditional() {
        let src = "lights/fire_walltorch_falloff_exp3 {
            {
                if  ( (time + parm4) * Parm3 >= 1.0 )
                forceHighQuality
                map    lights/falloff_exp2
                zeroClamp
            }
        }";

        let mat = Material::from_str(src).unwrap();
        assert_eq!(mat.stages.len(), 1);
        assert_eq!(mat.stages[0].image_compression, ImageCompress::ForceHigh);
        assert_eq!(mat.stages[0].image_path, "lights/falloff_exp2");
    }

    #[test]
    fn parse_custom_blend_mode() {
        let source = "gl_one, gl_dst_color";
        let mut lex = Tokeniser::from_str(source);

        let result = BlendMode::from_tokens(&mut lex).unwrap();
        match result {
            BlendMode::Custom(first, second) => {
                assert_eq!(first, "gl_one");
                assert_eq!(second, "gl_dst_color");
            }
            _ => panic!("incorrect blend mode"),
        }
    }

    #[test]
    fn parse_mtr_with_custom_blend_mode() {
        let source: &str = "test/blendmode {
                                {
                                    blend gl_one, gl_dst_color
                                    map path/to/blended/image.tga
                                }
                            }";

        let result = Material::from_str(source).unwrap();
        assert_eq!(result.stages.len(), 1);
        assert_eq!(
            result.stages[0].blend_mode,
            BlendMode::Custom(String::from("gl_one"), String::from("gl_dst_color"))
        );
        assert_eq!(result.stages[0].image_path, "path/to/blended/image.tga");
    }

    #[test]
    fn parse_description() {
        let carpet = r#"tdm_collision_carpet
                                    {
                                        surftype15
                                        description "carpet"
                                        collision
                                        forceopaque
                                    }"#;

        let result = Material::from_str(carpet).unwrap();
        assert_eq!(result.surface_type, Some(SurfaceType::SurfType15));
        assert_eq!(result.description, String::from("carpet"));
        assert_eq!(result.is_collision, true);
    }

    #[test]
    fn parse_unquoted_description() {
        let src = "textures/darkmod/nature/bones/bonepile_tiling_chared
                         {
                           surftype15
                           description gravel
                         }";

        let material = Material::from_str(&src).unwrap();
        assert_eq!(material.description, String::from("gravel"));
    }

    #[test]
    fn parse_unquoted_description_colliding_with_token() {
        let src = "textures/darkmod/volta/cauldron_relics_sheet01 {
            surftype15
            description stone
            noselfshadow
        }";

        let material = Material::from_str(&src).unwrap();
        assert_eq!(material.description, String::from("stone"));
    }

    #[test]
    fn parse_mtr_with_comments() {
        let mtr_with_comments = "//[****Graveyard-Materials****]
                                           tdm_skull
                                           {
                                              stone
                                              noshadows

                                              // This is the editor image
                                              qer_editorimage  models/darkmod/props/textures/skull01_ed
                                              bumpmap          models/darkmod/props/textures/skull01_local

                                              // Main diffuse map
                                              diffusemap       models/darkmod/props/textures/skull01
                                              specularmap      _black
                                           }";

        let result = Material::from_str(&mtr_with_comments).unwrap();

        assert_eq!(result.surface_type, Some(SurfaceType::Stone));
        assert_eq!(result.no_shadows, true);
        assert_eq!(
            result.editor_image,
            Some(String::from("models/darkmod/props/textures/skull01_ed"))
        );
    }

    #[test]
    fn parse_float() {
        let mut lex = Token::lexer("12.765");
        assert_eq!(lex.next(), Some(Token::Number(12.765)));

        let mut lex = Token::lexer("-1762.5");
        assert_eq!(lex.next(), Some(Token::Minus));
        assert_eq!(lex.next(), Some(Token::Number(1762.5)));

        let mut lex = Token::lexer("19287123");
        assert_eq!(lex.next(), Some(Token::Number(19287123.0)));

        let mut lex = Token::lexer("1.0.1");
        assert_eq!(lex.next(), Some(Token::Error));
    }

    #[test]
    fn parse_param() {
        let mut tok = Tokeniser::from_str("parm11");
        assert_eq!(tok.take(), Ok(Token::Parm(String::from("parm11"))));
    }

    #[test]
    fn tokenise_math_expression() {
        {
            let mut tok = Tokeniser::from_str("+");
            assert_eq!(tok.take(), Ok(Token::Plus));
        }

        {
            let mut tok = Tokeniser::from_str("2 + 7");
            assert_eq!(tok.take(), Ok(Token::Number(2.0)));
            assert_eq!(tok.take(), Ok(Token::Plus));
            assert_eq!(tok.take(), Ok(Token::Number(7.0)));
        }

        {
            let mut tok = Tokeniser::from_str("3+15");
            assert_eq!(tok.take(), Ok(Token::Number(3.0)));
            assert_eq!(tok.take(), Ok(Token::Plus));
            assert_eq!(tok.take(), Ok(Token::Number(15.0)));
        }

        {
            let mut tok = Tokeniser::from_str("-11.15 * 0.25");
            assert_eq!(tok.take(), Ok(Token::Minus));
            assert_eq!(tok.take(), Ok(Token::Number(11.15)));
            assert_eq!(tok.take(), Ok(Token::Times));
            assert_eq!(tok.take(), Ok(Token::Number(0.25)));
        }
    }

    /// Value provider which returns a constant value
    struct ConstantProvider {
        constant: f64,
        call_count: Cell<i32>,
    }

    impl ConstantProvider {
        fn new(constant: f64) -> ConstantProvider {
            ConstantProvider { constant, call_count: Cell::new(0) }
        }
    }

    impl ValueProvider for ConstantProvider {
        fn get_value(&self, _: &str) -> f64 {
            self.call_count.replace(self.call_count.get() + 1);
            self.constant
        }
        fn get_table_value(&self, _: &str, _: f64) -> f64 {
            self.call_count.replace(self.call_count.get() + 1);
            self.constant
        }
    }

    // MathExpr construction helpers
    impl MathExpr {
        fn try_from_str(s: &str) -> ParseResult<Self> {
            MathExpr::from_tokens(&mut Tokeniser::from_str(s))
        }
        fn from_str(s: &str) -> Self {
            Self::try_from_str(s).unwrap()
        }
    }

    #[test]
    fn parse_math_expression() {
        // Any valid float value is a match expression
        assert_eq!(MathExpr::from_str("1.234").try_value(), Some(1.234));
        assert_eq!(MathExpr::from_str("0").try_value(), Some(0.0));
        assert_eq!(MathExpr::from_str("-1").try_value(), Some(-1.0));
        assert_eq!(MathExpr::from_str("23456.0").try_value(), Some(23456.0));
        assert!(MathExpr::try_from_str("invalid").is_err());

        // Simple binary operation
        assert_eq!(MathExpr::from_str("1 + 3").try_value(), Some(4.0));
        assert_eq!(MathExpr::from_str("-12.383 * 15.68").try_value(), Some(-12.383 * 15.68));
        assert!(MathExpr::try_from_str("15 * * 9").is_err());
        assert_eq!(MathExpr::from_str("8 / 4").try_value(), Some(2.0));

        // Operations involving parameters
        let return5 = ConstantProvider::new(5.0);
        assert_eq!(MathExpr::from_str("0.40 * parm11").value(&return5), 2.0);
        assert_eq!(MathExpr::from_str("parm6 + 1.05").value(&return5), 6.05);
        assert_eq!(MathExpr::from_str("time * 2").value(&return5), 10.0);
        assert_eq!(MathExpr::from_str("sound*.5").value(&return5), 2.5);
        assert_eq!(MathExpr::from_str("parm0 * global0").value(&return5), 25.0);

        // Parenthesised expressions
        assert_eq!(MathExpr::from_str("(4 + 8)").try_value(), Some(12.0));
        assert_eq!(MathExpr::from_str("( 3 - 2 )").try_value(), Some(1.0));
        assert_eq!(MathExpr::from_str("( (time + parm4) * Parm3 )").value(&return5), 50.0);

        // Expressions should not require space separation
        assert_eq!(MathExpr::from_str("2+3").try_value(), Some(5.0));
        assert_eq!(MathExpr::from_str("parm1-8.5").value(&return5), -3.5);
        assert_eq!(MathExpr::from_str("-16*2").try_value(), Some(-32.0));
        assert_eq!(MathExpr::from_str("10+-8").try_value(), Some(2.0));
    }

    #[test]
    fn parse_boolean_expression() {
        assert_eq!(MathExpr::from_str("0.5 || 0").try_value(), Some(0.5));
        assert_eq!(MathExpr::from_str("0 || 128").try_value(), Some(128.0));

        let prov = ConstantProvider::new(15.0);
        assert_eq!(MathExpr::from_str("(parm5 || 8)").value(&prov), 15.0);
        assert_eq!(MathExpr::from_str("(0 || parm5)").value(&prov), 15.0);
    }

    #[test]
    fn parse_math_expression_with_tables() {
        let _expr = MathExpr::from_str("0.01 * sintable [ time * 0.1]");
    }

    #[test]
    fn parse_rgb_expression() {
        // Simple RGB multiplication
        let simple_src = "{
            blend filter
            rgb 0.65
        }";
        let stage = Stage::from_tokens(&mut Tokeniser::from_str(simple_src)).unwrap();
        assert_eq!(stage.rgba.r.try_value(), Some(0.65));
        assert_eq!(stage.rgba.g.try_value(), Some(0.65));
        assert_eq!(stage.rgba.b.try_value(), Some(0.65));
        assert_eq!(stage.rgba.a.try_value(), Some(1.0));

        // Expression involving a parameter
        let parm_src = "{
            blend add
            rgb 0.40 * parm11
        }";
        Stage::from_tokens(&mut Tokeniser::from_str(parm_src)).unwrap();
    }

    #[test]
    fn parse_alphatest() {
        let simple_src = "{
            blend diffusemap
            alphaTest 0.5
        }";
        Stage::from_tokens(&mut Tokeniser::from_str(simple_src)).unwrap();
    }

    #[test]
    fn parse_channel_masks() {
        // "maskcolor" only writes to alpha
        const MASK_COLOR: &str = "{
                                      maskcolor
                                      map textures/maps/alpha_only
                                  }";
        let maskcol = Stage::from_str(MASK_COLOR).unwrap();
        assert_eq!(maskcol.channels, Channel::A);
        assert_eq!(maskcol.image_path, String::from("textures/maps/alpha_only"));

        // "maskalpha" only writes to RGB
        const MASK_ALPHA: &str = "{
                                      maskAlpha
                                      map textures/maps/rgb_tex
                                  }";
        let maskalpha = Stage::from_str(MASK_ALPHA).unwrap();
        assert_eq!(maskalpha.channels, Channel::R | Channel::G | Channel::B);
        assert_eq!(maskalpha.image_path, String::from("textures/maps/rgb_tex"));
        assert_eq!(maskalpha.depth_write, true);

        // "maskDepth" masks the depth channel
        const MASK_DEPTH: &str = "{
            blend blend
            maskDepth
            colored
        }";
        let maskdepth = Stage::from_str(MASK_DEPTH).unwrap();
        assert_eq!(maskdepth.depth_write, false);
    }

    #[test]
    fn parse_image_expr() {
        // Simple image name
        let parse = |s| ImageExpr::from_str(s).unwrap();
        let expr = parse("textures/old_cloth_h.tga");
        assert_eq!(expr, ImageExpr::ImagePath(String::from("textures/old_cloth_h.tga")));
        assert_eq!(expr.get_images(), vec!["textures/old_cloth_h.tga"]);

        // addnormals and heightmap
        let expr = parse(
            "addnormals(textures/old_cloth, heightmap(textures/old_cloth_h.tga, 3 ) )",
        );
        assert!(std::matches!(expr, ImageExpr::AddNormals(_, _)));
        assert_eq!(expr.get_images(), vec!["textures/old_cloth", "textures/old_cloth_h.tga"]);

        // scale() can have a variable number of scale factors
        assert!(std::matches!(parse("scale(darkmod/straw_circle_local, 1, 1, 0, 1)"),
                ImageExpr::Scale(_, _)));
        assert!(std::matches!(parse("scale(darkmod/another_scale, 1)"),
                ImageExpr::Scale(_, _)));
        assert!(std::matches!(parse("scale(darkmod/another_scale, 1, 0.5)"),
                ImageExpr::Scale(_, _)));
        assert!(std::matches!(parse("scale(darkmod/another_scale,0,0,1)"),
                ImageExpr::Scale(_, _)));

        // Map expressions may use backslashes as separators
        assert_eq!(parse("textures\\particles\\lanternflare"),
                   ImageExpr::ImagePath(String::from("textures/particles/lanternflare")));

        // New expressions introduced by Dark Mod
        assert!(std::matches!(parse("cameraLayout(env/lights/defaultcm)"),
                ImageExpr::CameraLayout(_)));
    }

    #[test]
    fn parse_stage_with_transforms() {
        let src = "{
            blend diffusemap
            map textures/darkmod/bricks1_d
            scale 0.273, 1
            translate 0.5,-0.5
            shear 5, -17
            rotate -0.128
        }";

        let stage = Stage::from_str(src).unwrap();
        assert_eq!(stage.translate.x.try_value(), Some(0.5));
        assert_eq!(stage.translate.y.try_value(), Some(-0.5));
        assert_eq!(stage.scale.x.try_value(), Some(0.273));
        assert_eq!(stage.scale.y.try_value(), Some(1.0));
        assert_eq!(stage.shear.x.try_value(), Some(5.0));
        assert_eq!(stage.shear.y.try_value(), Some(-17.0));
        assert_eq!(stage.rotate.try_value(), Some(-0.128));
    }

    #[test]
    fn parse_stage_with_centerscale() {
        let src = "{
            blend   blend
            map     guis/assets/mission_failure/skull
            centerScale parm0, parm0
        }";

        let stage = Stage::from_str(src).unwrap();
        let prov = ConstantProvider::new(14.5);
        assert_eq!(stage.center_scale.x.value(&prov), 14.5);
        assert_eq!(stage.center_scale.y.value(&prov), 14.5);
        assert_eq!(prov.call_count.get(), 2);

        // centerScale does not affect scale
        assert_eq!(stage.scale.x.try_value(), Some(1.0));
        assert_eq!(stage.scale.y.try_value(), Some(1.0));
    }

    #[test]
    fn parse_clamp_mode() {
        {
            let default_clamp = "{
                blend bumpmap
                map textures/darkmod/normal_d.tga
            }";

            let mut tok = Tokeniser::from_str(default_clamp);
            let stage = Stage::from_tokens(&mut tok).unwrap();
            assert_eq!(stage.clamp_mode, ClampMode::None);
            assert_eq!(stage.alpha_zero_clamp, false);
        }

        {
            let zero_clamp = "{
                blend diffusemap
                map textures/whatever
                zeroClamp
            }";

            let mut tok = Tokeniser::from_str(zero_clamp);
            let stage = Stage::from_tokens(&mut tok).unwrap();
            assert_eq!(stage.clamp_mode, ClampMode::ZeroClamp);
            assert_eq!(stage.alpha_zero_clamp, false);
        }

        {
            let clamp = "{
                blend specularmap
                map textures/specular/spec_s
                colored
                clamp
                alphazeroclamp
            }";

            let mut tok = Tokeniser::from_str(clamp);
            let stage = Stage::from_tokens(&mut tok).unwrap();
            assert_eq!(stage.clamp_mode, ClampMode::Clamp);
            assert_eq!(stage.alpha_zero_clamp, true);
        }
    }

    #[test]
    fn parse_global_clamp_mode() {
        let src = "models/md5/chars/inventors/engineer2/head    //head03
        {
            qer_editorimage	models/md5/chars/inventors/engineer2/engineer_apron_ed
            noshadows
                clamp
                flesh

                bumpmap		models/md5/chars/inventors/engineer2/engineer_apron_local

            {blend        diffusemap
            map models/md5/chars/builders/forgertop_d
            rgb .7
            }
        }";

        let mut tok = Tokeniser::from_str(src);
        let mat = Material::from_tokens(&mut tok).unwrap();
        assert_eq!(mat.clamp_mode, ClampMode::Clamp);

        // Global clamp mode does not affect stage clamp mode
        assert_eq!(mat.stages.len(), 2);
        assert_eq!(mat.stages[1].blend_mode, BlendMode::Diffuse);
        assert_eq!(mat.stages[1].clamp_mode, ClampMode::None);
    }

    #[test]
    fn parse_shader_programs() {
        let src = "{
            map path/to/texture
            fragmentProgram heatHazeFrag.vfp
            vertexprogram heatHazeVert.vfp
        }";

        let mut tok = Tokeniser::from_str(src);
        let stage = Stage::from_tokens(&mut tok).unwrap();
        assert_eq!(stage.vertex_program, Some(String::from("heatHazeVert.vfp")));
        assert_eq!(stage.fragment_program, Some(String::from("heatHazeFrag.vfp")));

        // "program" sets both vertex and fragment program to same value
        let src = "{
            map second/tex
            Program blahBlah.vfp
        }";
        let stage = Stage::from_tokens(&mut Tokeniser::from_str(src)).unwrap();
        assert_eq!(stage.vertex_program.unwrap(), String::from("blahBlah.vfp"));
        assert_eq!(stage.fragment_program.unwrap(), String::from("blahBlah.vfp"));
    }

    #[test]
    fn parse_deform_expression() {
        let parse = |s| deform_expression(&mut Tokeniser::from_str(s));

        assert_eq!(parse("sprite"), Ok(Deform::Sprite));
        assert_eq!(parse("tube"), Ok(Deform::Tube));
        assert_eq!(parse("eyeball"), Ok(Deform::Eyeball));
        assert_eq!(parse("flare 6"), Ok(Deform::Flare(6.0)));
        assert_eq!(
            parse("particle particles/firstpart"),
            Ok(Deform::Particle(String::from("particles/firstpart")))
        );
        assert_eq!(
            parse("particle2 something"),
            Ok(Deform::Particle2(String::from("something")))
        );

        // deform turbulent <table> <range> <timeoffset> <domain>
        assert_eq!(
            parse("turbulent sinTable 0.0175 ( 0.25 * 2 ) 10"),
            Ok(Deform::Turbulent(String::from("sinTable"), 0.0175, 0.5, 10.0))
        );
    }

    #[test]
    fn parse_particle_macro() {
        let src = "textures/particles/drop2
                         {
                             PARTICLE_MACRO
                             qer_editorimage	textures/particles/drop2
                             {
                                 blend		add
                                 map			textures/particles/drop2
                                 vertexcolor
                             }
                         }";

        let mat = Material::from_str(&src).unwrap();
        assert_eq!(mat.stages.len(), 1);
        assert!(mat.no_self_shadow);
        assert!(mat.no_shadows);
        assert!(mat.translucent);
        assert!(mat.non_solid);
        assert!(mat.no_impact);
        assert!(mat.discrete);
    }

    #[test]
    fn parse_glass_macro() {
        let src = "textures/particles/pfirebig {
            GLASS_MACRO
            {
                blend   add
                map     textures/particles/pfirebig.tga
                vertexcolor
            }
        }";

        let mat = Material::from_str(src).unwrap();
        assert!(mat.two_sided);
        assert!(mat.no_shadows);
        assert!(mat.no_self_shadow);
        assert!(mat.translucent);
    }

    #[test]
    fn parse_fog_alpha() {
        let src = "tdm_potglass {
            glass
            qer_editorimage models/darkmod/props/textures/airpotion_s_ed
            fogAlpha 0.45
        }";

        let mat = Material::from_str(src).unwrap();
        assert_eq!(mat.fog_alpha, 0.45);
    }

    #[test]
    fn parse_polygon_offset() {
        let src = "textures/darkmod/decals/dirt/stainwall {
            polygonOffset 0.75 // this is 1 in DECAL_MACRO
            discrete
            sort decal
            noShadows
            qer_editorimage	textures/decals/stainwall.tga
        }";

        let mat = Material::from_str(&src).unwrap();
        assert_eq!(mat.polygon_offset, 0.75);
        assert!(mat.discrete);
        assert!(mat.no_shadows);
    }

    #[test]
    fn parse_material_with_deformation() {
        let source = "test/sprite {
            qer_editorimage textures/deform_ed
            deform sprite
        }";

        let mat = Material::from_str(source).unwrap();
        assert_eq!(mat.deform, Some(Deform::Sprite));
    }

    #[test]
    fn parse_spectrum() {
        let source = "test/spectrum {
            diffusemap textures/spectrum_d.tga
            spectrum 11
        }";

        let mat = Material::from_str(source).unwrap();
        assert_eq!(mat.spectrum, Some(11));
    }

    #[test]
    fn parse_light_types() {
        let non_light = Material::from_str(SIMPLE_MTR).unwrap();
        assert_eq!(non_light.light_type, None);

        let blend_light = Material::from_str(
            "test/lights/blend {
                blendLight
                noShadows
            }",
        )
        .unwrap();
        assert_eq!(blend_light.light_type, Some(LightType::Blend));
        assert_eq!(blend_light.no_shadows, true);
        assert_eq!(blend_light.no_portal_fog, false);

        let ambient_light = Material::from_str(
            "test/lights/ambient1 {
                ambientlight
            }",
        )
        .unwrap();
        assert_eq!(ambient_light.light_type, Some(LightType::Ambient));
        assert_eq!(ambient_light.no_shadows, false);

        let fog_light = Material::from_str(
            "test/lights/fog1 {
                fogLight
            }",
        )
        .unwrap();
        assert_eq!(fog_light.light_type, Some(LightType::Fog));
    }

    const BRIGHT_ROUND: &str = "lights/brightround
        {
            lightFalloffImage textures/lights/brightround
            {
                forceHighQuality
                map	textures/lights/brightround
                colored
                zeroClamp
            }
        }
    ";

    #[test]
    fn parse_light_falloff() {
        let non_light = Material::from_str(SIMPLE_MTR).unwrap();
        assert_eq!(non_light.light_falloff, None);

        let bright_round = Material::from_str(BRIGHT_ROUND).unwrap();
        assert_eq!(
            bright_round.light_falloff,
            Some(String::from("textures/lights/brightround"))
        );
    }

    #[test]
    fn parse_light_flags() {
        let src = "fogs/test/cloud_fog_noPortalFog {
            fogLight            // tell the engine it is fog and not a light
            noShadows
            noPortalFog
        }";

        let fog_light = Material::from_str(src).unwrap();
        assert!(fog_light.no_portal_fog);
    }

    const COLOR: &str = "shoulder_cowl_brown {
        flesh
        noshadows
        bumpmap        models/md5/chars/townsfolk/commoner/shoulders_local
        {
            blend diffusemap
            map	models/md5/chars/townsfolk/shoulders_green
            color   .9, .6, .6, 1  //color <redval>, <greenval>, <blueval>, <alphaval>
        }
    }";

    #[test]
    fn parse_color() {
        let shoulder_cowl = Material::from_str(COLOR).unwrap();
        assert_eq!(shoulder_cowl.stages.len(), 2);
    }

    #[test]
    fn parse_basic_table() {
        let src = "table flickertable_hi	{ { 0.91, -0.95, 0.92, -.98, 0.85 } } ";
        let table = Table::from_tokens(&mut Tokeniser::from_str(src)).unwrap();

        // Table metadata
        assert_eq!(table.name, "flickertable_hi");
        assert_eq!(table.clamp, false);
        assert_eq!(table.snap, false);

        // 5 values
        assert_eq!(table.values.len(), 5);
        assert_eq!(table.values[0], 0.91);
        assert_eq!(table.values[1], -0.95);
        assert_eq!(table.values[2], 0.92);
        assert_eq!(table.values[3], -0.98);
        assert_eq!(table.values[4], 0.85);
    }

    #[test]
    fn parse_table_with_flags() {
        let src = "table sosTable { snap clamp { 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1 } }";
        let table = Table::from_tokens(&mut Tokeniser::from_str(src)).unwrap();

        // Metadata
        assert_eq!(table.name, "sosTable");
        assert_eq!(table.clamp, true);
        assert_eq!(table.snap, true);

        // Data
        assert_eq!(table.values.len(), 13);
        assert_eq!(table.values[0], 1.0);
        assert_eq!(table.values[1], 0.0);
        assert_eq!(table.values[12], 1.0);
    }

    #[test]
    fn parse_tables_in_mtr() {
        let src = "
            /* Define various tables that are nec. to get certain shader effects working: */

            table sosTable { snap { 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0 } }
            table fadeTable { { 0, 1 } }

            some/material {
                diffusemap some/diffusemap.tga
            }
            table oneThirdTable { { 0, 1, 0 } }

            // sinTable and cosTable must be defined for the rotate function to work
        ";

        let parsed = ParsedMaterials::new_from_str(src);
        assert_eq!(parsed.materials.len(), 1);
        assert_eq!(parsed.last_error, None);

        // 3 parsed tables with names
        assert_eq!(parsed.tables.len(), 3);
        assert_eq!(parsed.tables[0].name, "sosTable");
        assert_eq!(parsed.tables[1].name, "fadeTable");
        assert_eq!(parsed.tables[2].name, "oneThirdTable");
    }

    #[test]
    fn parse_vector4() {
        // X only
        let parsevec = |s: &str| Vector4::from_tokens(&mut Tokeniser::from_str(&s)).unwrap();
        let vec1 = parsevec("24.5");
        assert_eq!(vec1.try_value(), Some([24.5, 0.0, 0.0, 1.0]));

        // X and Y only
        let vec2 = parsevec("13, 18.8");
        assert_eq!(vec2.try_value(), Some([13.0, 18.8, 0.0, 1.0]));

        // X, Y and Z only
        let vec3 = parsevec(" -16 , 150,  5.25");
        assert_eq!(vec3.try_value(), Some([-16.0, 150.0, 5.25, 1.0]));

        // All components
        let vec4 = parsevec("100.8, -30.25 , 16, 897");
        assert_eq!(vec4.try_value(), Some([100.8, -30.25, 16.0, 897.0]));

        // Non-constant expression
        let vec5 = parsevec("time * 3, time + 8.2");
        assert_eq!(vec5.try_value(), None);

        let prov = ConstantProvider::new(5.0);
        assert_eq!(vec5.x.value(&prov), 15.0);
        assert_eq!(vec5.y.value(&prov), 13.2);
        assert_eq!(vec5.z.value(&prov), 0.0);
        assert_eq!(vec5.w.value(&prov), 1.0);
    }

    #[test]
    fn parse_vertexparm() {
        let src = "numberwheel_glass
        {
            qer_editorimage	textures/darkmod/sfx/whiteglass_ed
            {	// the morphing effect
                vertexParm              0       time * 0 , time * 0 // texture scrolling
                vertexParm              1       .15         // magnitude of the distortion
            }
        }";

        let mat = Material::from_str(src).unwrap();
        assert_eq!(
            mat.editor_image.unwrap(),
            String::from("textures/darkmod/sfx/whiteglass_ed")
        );
        assert_eq!(mat.stages.len(), 1);
        assert_eq!(mat.stages[0].vertex_parms.len(), 2);
        assert_eq!(mat.stages[0].vertex_parms[&0].try_value(), None);
        assert_eq!(mat.stages[0].vertex_parms[&1].try_value(), Some([0.15, 0.0, 0.0, 1.0]));
    }

    #[test]
    fn parse_image_layouts() {
        {
            let normal = "{ map path/to/texture }";
            let stage = Stage::from_str(normal).unwrap();
            assert_eq!(stage.image_layout, ImageLayout::Flat);
        }
        {
            let cubemap = "{
                cubeMap path/to/cube_map
            }";
            let stage = Stage::from_str(cubemap).unwrap();
            assert_eq!(stage.image_layout, ImageLayout::Cube);
        }
        {
            let camcube = "{
                cameraCubemap images/cubes/camera_cube
            }";
            let stage = Stage::from_str(camcube).unwrap();
            assert_eq!(stage.image_layout, ImageLayout::CameraCube);
        }
    }

    #[test]
    fn parse_filtering() {
        let nearest = "{
            blend blend
            colored
            nearest
            map textures/consolefont_24
        }";
        let stage = Stage::from_str(nearest).unwrap();
        assert_eq!(stage.image_filter, ImageFilter::Nearest);
        assert_eq!(stage.no_pic_mip, false);

        let nopicmip = "{
            blend blend
            colored
            nopicmip
            linear
            map textures/consolefont
        }";
        let stage = Stage::from_str(nopicmip).unwrap();
        assert_eq!(stage.image_filter, ImageFilter::Linear);
        assert_eq!(stage.no_pic_mip, true);
    }

    #[test]
    fn parse_compression() {
        let normal = "{
            blend diffusemap
            map textures/whatever
        }";
        let stage = Stage::from_str(normal).unwrap();
        assert_eq!(stage.image_compression, ImageCompress::Normal);

        let high = "{
            blend diffusemap
            map textures/whatever
            highQuality
        }";
        let stage = Stage::from_str(high).unwrap();
        assert_eq!(stage.image_compression, ImageCompress::High);

        let high = "{
            blend diffusemap
            map textures/whatever
            forcehighquality
        }";
        let stage = Stage::from_str(high).unwrap();
        assert_eq!(stage.image_compression, ImageCompress::ForceHigh);
    }

    impl FragmentMap {
        fn from_str(s: &str) -> Self {
            let mut tok = Tokeniser::from_str(s);
            Self::from_tokens(&mut tok).unwrap()
        }
    }

    #[test]
    fn parse_fragment_map() {
        // Simple image map
        let frag = FragmentMap::from_str("_currentRender");
        assert_eq!(frag.image_path, "_currentRender");

        // Quality options
        let force_high = FragmentMap::from_str("forceHighQuality 	_currentRender");
        assert_eq!(force_high.image_compression, ImageCompress::ForceHigh);

        // Filtering options
        let linear = FragmentMap::from_str(" linear _bloomImage");
        assert_eq!(linear.image_filter, ImageFilter::Linear);
    }

    #[test]
    fn parse_stage_with_fragment_maps() {
        let source = "	{
            vertexProgram     rotoedge.vfp
                vertexParm 0 	1.5
            fragmentProgram   rotoedge.vfp
            fragmentMap       0                 _currentRender
        }";
        let stage = Stage::from_str(source).unwrap();
        assert_eq!(stage.fragment_maps.len(), 1);
        assert_eq!(stage.fragment_maps[&0].image_path, "_currentRender");
    }

    #[test]
    fn parse_texgen() {
        let none = "	{
            map _white
        }";
        let stage = Stage::from_str(none).unwrap();
        assert!(stage.texgen.is_none());

        let reflect = "{
            blend           add
            cameraCubeMap   env/dome
            texgen          skybox
        }";
        let stage = Stage::from_str(reflect).unwrap();
        assert_eq!(stage.texgen.unwrap(), TexGen::Skybox);

        let wobble = "{
            forceHighQuality
            blend add
            cameraCubeMap env/skyboxes/dm_sqsky/dm_sqsky
            texgen skybox
            texgen wobblesky .0 .0 .0
        }";
        let stage = Stage::from_str(wobble).unwrap();
        let zero = MathExpr::constant(0.0);
        assert_eq!(stage.texgen.unwrap(), TexGen::WobbleSky(zero.clone(), zero.clone(), zero));
    }

    #[test]
    fn parse_renderbump() {
        let src = "textures/darkmod/renderbump {
            renderbump -size 512 768 -aa 2 -trace 0.5 -colorMap models/mytestmodels/ericface_local.tga models/mytestmodels/eric_hi.lwo
        }";

        let mat = Material::from_str(&src).unwrap();
        let rb = mat.render_bump.unwrap();
        assert_eq!(rb.aa_level, 2);
        assert_eq!(rb.width, 512);
        assert_eq!(rb.height, 768);
        assert_eq!(rb.trace, 0.5);
        assert_eq!(rb.image, String::from("models/mytestmodels/ericface_local.tga"));
        assert_eq!(rb.model, String::from("models/mytestmodels/eric_hi.lwo"));
    }

    #[test]
    fn parse_remote_rendermap() {
        let src = "textures/darkmod/camera {
            qer_editorimage textures/editor/cameragui.tga
            noshadows
            {
                red Parm0
                green Parm1
                blue Parm2
                remoteRenderMap 232 232 // width / height of render image, ie resolution of screen
                map \"_camera1\"
                scale -1, -1
                translate -1, -1
            }
        }";

        let mat = Material::from_str(&src).unwrap();
        assert_eq!(mat.stages.len(), 1);
        assert_eq!(
            mat.stages[0].remote_render_map,
            Some(BufferSize { width: 232, height: 232 })
        );
        assert_eq!(mat.stages[0].image_path, String::from("_camera1"));
    }

    #[test]
    fn parse_frobstage() {
        let src = "tdm_roughcloth_brown {
            diffusemap models/md5/chars/cloth_rough_generic_brown
            frobstage_texture textures/cloth_rough_generic01 0.15 0.40
        }";

        let mat = Material::from_str(&src).unwrap();
        assert_eq!(
            mat.frob_stage,
            FrobStage::ImageMap(String::from("textures/cloth_rough_generic01"), 0.15, 0.4)
        );
    }

    #[test]
    fn parse_decalinfo() {
        // Ensure we can parse the RGBA values
        let mut tok = Tokeniser::from_str("0.5 0.75 0.25 1");
        let rgba = RGBA::from_tokens(&mut tok).unwrap();
        assert_eq!(rgba, RGBA { r: 0.5, g: 0.75, b: 0.25, a: 1.0 });

        let src = "textures/decals/blood01 {
            DECAL_MACRO
            decalInfo 0.5 0 ( 5 5 5 1 ) ( 0 0 0 0 )
        }";

        let mat = Material::from_str(&src).unwrap();
        let decal_info = mat.decal_info.unwrap();
        assert_eq!(decal_info.stay_secs, 0.5);
        assert_eq!(decal_info.fade_secs, 0.0);
        assert_eq!(decal_info.start_rgba, RGBA { r: 5.0, g: 5.0, b: 5.0, a: 1.0 });
        assert_eq!(decal_info.end_rgba, RGBA { r: 0.0, g: 0.0, b: 0.0, a: 0.0 });

        // Test with negative RGB values
        let src = "textures/darkmod/decals/airship/cannonball_impact {
            DECAL_MACRO
            decalInfo 10 30 ( 1 1 1 10 ) ( -5 -5 -5 0 )
        }";
        let mat = Material::from_str(&src).unwrap();
        let decal_info = mat.decal_info.unwrap();
        assert_eq!(decal_info.stay_secs, 10.0);
        assert_eq!(decal_info.fade_secs, 30.0);
        assert_eq!(decal_info.start_rgba, RGBA { r: 1.0, g: 1.0, b: 1.0, a: 10.0 });
        assert_eq!(decal_info.end_rgba, RGBA { r: -5.0, g: -5.0, b: -5.0, a: 0.0 });
    }

    #[test]
    fn parse_mirror_render_map() {
        let src = "textures/washroom/mirror {
            noshadows
            glass
            qer_editorimage	textures/glass/mirror
            {
                mirrorRenderMap 512 256
                translate   0.5, 0.5
                scale       0.5, 0.5
            }
        }";

        let mat = Material::from_str(&src).unwrap();
        assert_eq!(mat.stages.len(), 1);
        assert_eq!(
            mat.stages[0].mirror_render_map,
            Some(BufferSize { width: 512, height: 256 })
        );
    }

    #[test]
    fn parse_guisurf() {
        let src = "textures/common/pda_gui {
           qer_editorimage   textures/editor/entityGui.tga
           discrete
           guiSurf  guis/lvlmaps/genericmap.gui
        }";

        let mat = Material::from_str(&src).unwrap();
        assert_eq!(mat.gui_surf, Some(String::from("guis/lvlmaps/genericmap.gui")));
    }

    #[test]
    fn parse_looping_videomap() {
        let src = "video/tdm_briefing_video {
            qer_editorimage	textures/editor/video
            {
                videoMap    loop video/briefing_video.roq
            }
        }";

        let mat = Material::from_str(&src).unwrap();
        assert_eq!(mat.stages.len(), 1);
        let vm = mat.stages[0].video_map.as_ref().unwrap();
        assert!(vm.is_looping);
        assert_eq!(vm.video_path, String::from("video/briefing_video.roq"));
    }

    #[test]
    fn parse_nonlooping_videomap() {
        let src = "guis/video/credits/walk_sequence {
            {
                blend       add
                videoMap    video/credits/walk01.roq
            }
        }";

        let mat = Material::from_str(src).unwrap();
        assert_eq!(mat.stages.len(), 1);
        let vm = mat.stages[0].video_map.as_ref().unwrap();
        assert!(!vm.is_looping);
        assert_eq!(vm.video_path, String::from("video/credits/walk01.roq"));
    }

    #[test]
    fn parse_area_portal() {
        let src = "textures/editor/visportal {
            qer_editorimage	textures/editor/visportal.tga
            areaportal
            noshadows
        }";

        let mat = Material::from_str(src).unwrap();
        assert!(mat.is_area_portal);
        assert!(!mat.no_carve);
    }

    #[test]
    fn parse_nocarve() {
        let src = "textures/editor/aassolid {
            qer_editorimage	textures/editor/aassolid.tga
            qer_nocarve		// don't let an awry CSG operation cut it up
        }";

        let mat = Material::from_str(src).unwrap();
        assert!(!mat.is_area_portal);
        assert!(mat.no_carve);
    }

    #[test]
    fn parse_aas_solid() {
        let src = "textures/editor/aassolid {
            nonsolid
            aassolid
        }";

        let mat = Material::from_str(src).unwrap();
        assert!(!mat.aas_obstacle);
        assert!(mat.aas_solid);
    }

    #[test]
    fn parse_aas_obstacle() {
        let src = "textures/editor/aasobstacle {
            qer_editorimage	textures/editor/aasobstacle.tga
            noshadows
            aasobstacle
        }";

        let mat = Material::from_str(src).unwrap();
        assert!(!mat.aas_solid);
        assert!(mat.aas_obstacle);
    }
}
