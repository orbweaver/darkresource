//! Common functionality for parsing code
use std::{
    cell::{Cell, RefCell},
    fmt::{Debug, Display},
    ops::Range,
};

use logos::{Lexer, Logos};

/// Error returned when parsing fails or reaches the end of the token stream.
///
/// In some contexts NoMoreTokens isn't actually an error (every token stream must end at some
/// point), so higher-level code may discard this ParseError and return a success value
/// instead.
#[derive(PartialEq, Eq, Debug)]
pub enum ParseError {
    NoMoreTokens,
    UnexpectedToken(String, Option<Range<usize>>),
}

impl Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for ParseError {}

impl ParseError {
    /// Construct a ParseError::UnexpectedToken from the lexer's current token
    fn from_current_slice<'source, T>(lex: &Lexer<'source, T>) -> Self
    where
        T: Logos<'source, Source = str>,
    {
        ParseError::UnexpectedToken(String::from(lex.slice()), Some(lex.span()))
    }

    /// Construct a ParseError::UnexpectedToken from a single given token
    pub fn from_tok<'source, T>(tok: &T) -> Self
    where
        T: Logos<'source, Source = str> + Debug,
    {
        ParseError::UnexpectedToken(format!("{:?}", tok), None)
    }
}

pub type ParseResult<T> = Result<T, ParseError>;

/// Generic object to extract and return Tokens from a suitable Logos enum. Provides
/// convenience methods to assert that the next token is a specific token, and to "peek" the
/// next token without discarding it.
pub struct GenericTokeniser<'source, T: Logos<'source> + PartialEq> {
    // Underlying lexer supplying tokens
    lexer: RefCell<Lexer<'source, T>>,

    // Last peeked token, if any
    peeked: Cell<Option<T>>,
}

impl<'source, T> GenericTokeniser<'source, T>
where
    T: Logos<'source, Source = str> + PartialEq + Clone,
{
    // Construct from a Lexer
    fn from_lexer(lexer: Lexer<'source, T>) -> Self {
        Self { lexer: RefCell::new(lexer), peeked: Cell::new(None) }
    }

    // Construct from a string source
    pub fn from_str(s: &'source T::Source) -> Self
    where
        T::Extras: Default,
    {
        Self::from_lexer(T::lexer(s))
    }

    /// Return the next token from the lexer, or ParseError::NoMoreTokens if there is none
    pub fn take(&mut self) -> ParseResult<T> {
        self.try_take().ok_or(ParseError::NoMoreTokens)
    }

    /// Return the next token from the lexer, or None if there is none
    pub fn try_take(&mut self) -> Option<T> {
        if let Some(tok) = self.peeked.take() {
            // Return the last peeked token if present
            Some(tok)
        } else {
            // Nothing peeked, just consume the next token from the lexer
            self.lexer.borrow_mut().next()
        }
    }

    /// Assert that the next token is `tok`, returning it as-is if correct, otherwise UnexpectedToken
    pub fn assert(&mut self, tok: T) -> ParseResult<T> {
        let next_tok = self.take()?;
        if next_tok == tok {
            Ok(next_tok)
        } else {
            Err(self.unexpected())
        }
    }

    /// Discard the next token without returning it
    pub fn discard(&mut self) {
        drop(self.take());
    }

    /// Get the string slice corresponding to the last token
    pub fn slice(&self) -> &str {
        self.lexer.borrow().slice()
    }

    /// Peek the next token without consuming. Multiple calls return the same token.
    pub fn peek(&self) -> Option<T> {
        // Pull the next token from the lexer
        let mut peeked = self.peeked.take();
        if peeked.is_none() {
            peeked = self.lexer.borrow_mut().next();
        }

        // Store the peeked token. We must clone() here because we are returning a value not
        // a reference, and we cannot move out of self.peeked because peek() should be
        // idempotent and non-mutating.
        self.peeked.set(peeked.clone());

        // Return what was peeked (either Some or None)
        peeked
    }

    /// Put back an already-taken token.
    ///
    /// The token will be inserted into the slot used for peek(), and will panic if there is
    /// already a peeked token in the slot. Therefore this method should not be called more
    /// than once in succession, or after a previous call to peek().
    pub fn push(&mut self, tok: T) {
        if let Some(_) = self.peeked.take() {
            panic!("Tokeniser::push() would overwrite peeked token");
        }

        self.peeked.set(Some(tok));
    }

    // Create an unexpected error from the current slice
    pub fn unexpected(&self) -> ParseError {
        let lexer = self.lexer.borrow();
        ParseError::from_current_slice(&lexer)
    }
}
