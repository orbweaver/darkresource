use std::path::PathBuf;

use criterion::*;
use darkresource::Installation;
use once_cell::sync::Lazy;

// Filesystem path to main testdata directory
static BASE_DIR: Lazy<PathBuf> = Lazy::new(|| {
    let mut basedir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    basedir.push("testdata/base");
    basedir
});

struct TestFile {
    path: &'static str,
    size: u64,
}

// Benchmarks for initial setup
fn initialisation_benchmark(crit: &mut Criterion) {
    crit.bench_function("create_installation", |b| {
        b.iter(|| {
            let _ = black_box(Installation::create_from_path(&BASE_DIR).unwrap());
        })
    });
}

// Benchmarks for reading contents from PK4s
fn read_data_benchmark(crit: &mut Criterion) {
    let mut inst = Installation::create_from_path(&BASE_DIR).unwrap();
    let mut readgroup = crit.benchmark_group("read_data");

    // Loading file contents
    let test_files = [
        TestFile {
            path: "models/cubes/Cube.ase",
            size: 8509,
        },
        TestFile {
            path: "materials/tdm_models_kitchen.mtr",
            size: 40492,
        },
    ];
    fn get_contents(ins: &mut Installation, testfile: &TestFile) {
        let contents = ins.get_contents(testfile.path).unwrap();
        assert_eq!(contents.len() as u64, testfile.size);
    }
    for f in test_files {
        readgroup.throughput(Throughput::Bytes(f.size));
        readgroup.bench_with_input("get_contents", &f, |b, testfile| {
            b.iter(|| get_contents(&mut inst, testfile))
        });
    }

    // Searching for files in the tree
    const NUM_MTRS: u64 = 52;
    readgroup.throughput(Throughput::Elements(NUM_MTRS));
    readgroup.bench_function("find_files", |b| {
        b.iter(|| {
            let files = inst.find_files("materials/*.mtr").unwrap();
            assert_eq!(files.len() as u64, NUM_MTRS);
        })
    });

    readgroup.finish();
}

// Benchmarks for material parsing
fn mtr_parsing_benchmark(crit: &mut Criterion) {
    let inst = Installation::create_from_path(&BASE_DIR).unwrap();
    let mut group = crit.benchmark_group("parse_defs");

    const NUM_MATS: usize = 848;
    group.throughput(Throughput::Elements(NUM_MATS as u64));
    group.bench_function("list_material_names", |b| {
        b.iter(|| {
            let parsed = inst.get_materials();
            assert_eq!(parsed.decls.len(), NUM_MATS);
        })
    });

    group.finish();
}

criterion_group!(
    name = benches;
    config = Criterion::default().noise_threshold(0.10);
    targets = initialisation_benchmark, read_data_benchmark, mtr_parsing_benchmark
);
criterion_main!(benches);
